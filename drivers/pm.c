#include "drivers/pm.h"
#include "drivers/sysctrl.h"

void pm_reset_mcu()
{
    NVIC_SystemReset();
}

void pm_enter_mode(pm_mode mode, bool return_to_sleep_after_interrupt)
{
    if(mode >= MODE_ACTIVE) { return; }

    while(!sysctrl_can_enter_standby());

    if(return_to_sleep_after_interrupt)
    {
        SCB->SCR |= 1; // SLEEPONEXIT = 1
    }
    else
    {
        SCB->SCR &= ~1; // SLEEPONEXIT = 0
    }

    if(mode == MODE_STANDBY)
    {
        SCB->SCR |= 2; // SLEEPDEEP = 1
    }
    else
    {
        SCB->SCR &= ~2; // SLEEPDEEP = 0
        PM->SLEEP.bit.IDLE = mode;
    }

    __WFI();
}

void pm_set_prescalar(pm_clk clk, pm_clock_prescalar prescalar)
{
    while(!PM->INTFLAG.bit.CKRDY); // if you change a prescalar while this is zero, the system becomes unstable or hangs

    if(clk == PM_CLK_CPU)
    {
        PM->CPUSEL.bit.CPUDIV = prescalar;
    }
    else if(clk == PM_CLK_APB_A)
    {
        *((uint8_t*)&PM->APBASEL.reg) = (uint8_t)prescalar; // must be 8-bit access
    }
    else if(clk == PM_CLK_APB_B)
    {
        *((uint8_t*)&PM->APBBSEL.reg) = (uint8_t)prescalar; // must be 8-bit access
    }
    else if(clk == PM_CLK_APB_C)
    {
        *((uint8_t*)&PM->APBCSEL.reg) = (uint8_t)prescalar; // must be 8-bit access
    }

    while(!PM->INTFLAG.bit.CKRDY);
}

pm_reset_source pm_get_reset_source()
{
    if(PM->RCAUSE.bit.POR) { return RESET_POR; }
    if(PM->RCAUSE.bit.BOD12) { return RESET_BOD12; }
    if(PM->RCAUSE.bit.BOD33) { return RESET_BOD33; }
    if(PM->RCAUSE.bit.EXT) { return RESET_EXTERNAL; }
    if(PM->RCAUSE.reg & 0x20) { return RESET_WDT; } // the WDT #define messes with this, so I have to do it this way
    if(PM->RCAUSE.bit.SYST) { return RESET_CPU; }
    return RESET_UNKNOWN;
}

void pm_set_peripheral_clk_enabled(pm_peripheral_clk pclk, bool enabled)
{
    int group = (pclk & 0xC0) >> 6;
    int mask = 1 << (pclk & 0x3F);

    volatile uint32_t* target = NULL;

    if(group == 0)       // AHB group
    {
        target = &PM->AHBMASK.reg;
    }
    else if(group == 1)  // APBA group
    {
        target = &PM->APBAMASK.reg;
    }
    else if(group == 2)  // APBB group
    {
        target = &PM->APBBMASK.reg;
    }
    else if(group == 3)  // APBC group
    {
        target = &PM->APBCMASK.reg;
    }
    else
    {
        return;
    }

    if(enabled)
    {
        *target |= mask;
    }
    else
    {
        *target &= ~mask;
    }
}
