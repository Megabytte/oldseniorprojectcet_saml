#ifndef _DRIVERS_DELAY_H_
#define _DRIVERS_DELAY_H_

#include "drivers/defs.h"

void delay_ms(uint32_t ms);
uint64_t delay_get_time_ms(void);

#endif /* _DRIVERS_DELAY_H_ */
