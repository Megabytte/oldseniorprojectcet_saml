#include "drivers/delay.h"

volatile uint64_t time_ms = 0;
void SysTick_Handler()
{
    time_ms++;
}

void delay_ms(uint32_t ms)
{
    uint64_t end_time = time_ms + ms;
    while(end_time > time_ms);
}

uint64_t delay_get_time_ms()
{
    return time_ms;
}
