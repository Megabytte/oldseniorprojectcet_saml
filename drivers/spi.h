#ifndef _DRIVERS_SPI_H_
#define _DRIVERS_SPI_H_

#include "drivers/defs.h"

typedef enum
{
    SPI_OP_MODE_SLAVE = 0x2,
    SPI_OP_MODE_MASTER = 0x3
} spi_op_mode;

typedef enum
{
    SPI_DATA_ORDER_MSB = 0x0,
    SPI_DATA_ORDER_LSB = 0x1
} spi_data_order;

typedef enum
{
    SPI_CLK_MODE_0 = 0x0, // CPOL: 0, CPHA: 0
    SPI_CLK_MODE_1 = 0x1, // CPOL: 0, CPHA: 1
    SPI_CLK_MODE_2 = 0x2, // CPOL: 1, CPHA: 0
    SPI_CLK_MODE_3 = 0x3  // CPOL: 1, CPHA: 1
} spi_clock_mode;

typedef enum
{
    SPI_PINOUT_A = 0x00, // DI: PAD[0], DO: PAD[0], SCK: PAD[1], SS: PAD[2], Note: Loop-back
    SPI_PINOUT_B = 0x01, // DI: PAD[0], DO: PAD[2], SCK: PAD[3], SS: PAD[1], Note:  Unique
    SPI_PINOUT_C = 0x02, // DI: PAD[0], DO: PAD[3], SCK: PAD[1], SS: PAD[2], Note:  Unique
    SPI_PINOUT_D = 0x03, // DI: PAD[0], DO: PAD[0], SCK: PAD[3], SS: PAD[1], Note:  Loop-back
    SPI_PINOUT_E = 0x10, // DI: PAD[1], DO: PAD[0], SCK: PAD[1], SS: PAD[2], Note:  Broken
    SPI_PINOUT_F = 0x11, // DI: PAD[1], DO: PAD[2], SCK: PAD[3], SS: PAD[1], Note:  No HW SS
    SPI_PINOUT_G = 0x12, // DI: PAD[1], DO: PAD[3], SCK: PAD[1], SS: PAD[2], Note:  Broken
    SPI_PINOUT_H = 0x13, // DI: PAD[1], DO: PAD[0], SCK: PAD[3], SS: PAD[1], Note:  No HW SS
    SPI_PINOUT_I = 0x20, // DI: PAD[2], DO: PAD[0], SCK: PAD[1], SS: PAD[2], Note:  No HW SS
    SPI_PINOUT_J = 0x21, // DI: PAD[2], DO: PAD[2], SCK: PAD[3], SS: PAD[1], Note:  Loop-back
    SPI_PINOUT_K = 0x22, // DI: PAD[2], DO: PAD[3], SCK: PAD[1], SS: PAD[2], Note:  No HW SS
    SPI_PINOUT_L = 0x23, // DI: PAD[2], DO: PAD[0], SCK: PAD[3], SS: PAD[1], Note:  Unique
    SPI_PINOUT_M = 0x30, // DI: PAD[3], DO: PAD[0], SCK: PAD[1], SS: PAD[2], Note:  Unique
    SPI_PINOUT_N = 0x31, // DI: PAD[3], DO: PAD[2], SCK: PAD[3], SS: PAD[1], Note:  Broken
    SPI_PINOUT_O = 0x32, // DI: PAD[3], DO: PAD[3], SCK: PAD[1], SS: PAD[2], Note:  Loop-back
    SPI_PINOUT_P = 0x33  // DI: PAD[3], DO: PAD[0], SCK: PAD[3], SS: PAD[1], Note:  Broken
} spi_pinout;

typedef enum
{
    SPI_FORMAT_SPI_FRAME              = 0x0,
    SPI_FORMAT_SPI_FRAME_WITH_ADDRESS = 0x2
} spi_frame_format;

typedef enum
{
    SPI_ADDRESS_MODE_MASK    = 0x0,
    SPI_ADDRESS_MODE_2_ADDRS = 0x1,
    SPI_ADDRESS_MODE_RANGE   = 0x2
} spi_address_mode;

typedef enum
{
    SPI_CHAR_SIZE_8 = 0x0,
    SPI_CHAR_SIZE_9 = 0x1
} spi_character_size;

typedef enum
{
    SPI_INT_DRE = 0x01, // Data Register Empty
    SPI_INT_TXC = 0x02, // Transmit Complete
    SPI_INT_RXC = 0x04, // Receive Complete
    SPI_INT_SSL = 0x08, // Slave Select Low
    SPI_INT_ERR = 0x80  // Error
} spi_interrupt;

void spi_set_op_mode(Sercom* spi, spi_op_mode mode);
void spi_set_clock_mode(Sercom* spi, spi_clock_mode mode);
void spi_set_data_order(Sercom* spi, spi_data_order order);
void spi_set_pinout(Sercom* spi, spi_pinout pinout);
void spi_set_frame_format(Sercom* spi, spi_frame_format format);
void spi_set_character_size(Sercom* spi, spi_character_size ch_size);
void spi_slave_set_address_mode(Sercom* spi, spi_address_mode mode);
void spi_slave_set_address_value(Sercom* spi, uint8_t primary_address, uint8_t address_mask_or_secondary_address_or_unused);
void spi_slave_set_select_low_detection_enabled(Sercom* spi, bool enabled);
void spi_slave_set_data_preload_enabled(Sercom* spi, bool enabled);
void spi_master_set_baudrate(Sercom* spi, uint32_t sercom_clk_rate, uint32_t desired_baudrate);
uint32_t spi_master_get_baudrate(Sercom* spi, uint32_t sercom_clk_rate);
void spi_master_set_hardware_slave_select_enabled(Sercom* spi, bool enabled);
void spi_set_receiver_enabled(Sercom* spi, bool enabled);
void spi_set_immediate_buffer_overflow_notification_enabled(Sercom* spi, bool enabled);
void spi_set_run_in_standby_enabled(Sercom* spi, bool enabled);
void spi_enable_interrupt(Sercom* spi, spi_interrupt interrupt);
void spi_disable_interrupt(Sercom* spi, spi_interrupt interrupt);
void spi_set_enabled(Sercom* spi, bool enabled);
void spi_swreset(Sercom* spi);

void spi_master_sync_transfer(Sercom* spi, uint16_t* data_out, uint16_t* data_in, uint32_t data_length);
void spi_master_async_transfer(Sercom* spi, uint16_t* data_out, uint16_t* data_in, uint32_t data_length);

void spi_master_interrupt_handler(Sercom* spi);
// example usage: void SERCOM0_Handler() { spi_master_interrupt_handler(SERCOM0); }

void spi_slave_interrupt_handler(Sercom* spi);
// example usage: void SERCOM0_Handler() { spi_slave_interrupt_handler(SERCOM0); }

#endif /* _DRIVERS_SPI_H_ */
