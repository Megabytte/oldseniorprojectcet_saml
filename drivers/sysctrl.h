#ifndef _DRIVERS_SYSCTRL_H_
#define _DRIVERS_SYSCTRL_H_

#include "drivers/defs.h"

/*
Boot State:
    OSC8M is enabled and divided by 8
    Generic Generator 0 uses OSC8M as source and generates GCLK_MAIN
    CPU and BUS clocks are undivided

    OSCULP32K is enabled and undivided
    Generic Generator 2 uses OSCULP32K as source without division
    WDT Generic Clock uses Generator 2 as source
*/

typedef enum
{
    CLOCK_XOSC,
    CLOCK_XOSC32K,
    CLOCK_OSC32K,
    CLOCK_OSCULP32K,
    CLOCK_OSC8M,
    CLOCK_DFLL48M,
    CLOCK_FDPLL96M
} sysctrl_clock;

typedef enum
{
    EXT_CLOCK_XOSC,
    EXT_CLOCK_XOSC32K
} sysctrl_ext_clock;

typedef enum
{
    CLOCK_32K_XOSC32K,
    CLOCK_32K_OSC32K
} sysctrl_32k_clock;

typedef enum
{
    INT_CLOCK_OSC32K,
    INT_CLOCK_OSCULP32K,
    INT_CLOCK_OSC8M,
    INT_CLOCK_DFLL48M,
    INT_CLOCK_FDPLL96M
} sysctrl_int_clock;

typedef enum
{
    MODE_EXT_CLOCK   = 0x0,
    MODE_EXT_CRYSTAL = 0x1
} sysctrl_ext_mode;

typedef enum
{
    XOSC_STARTUP_TIME_1     = 0x0, // 31us
    XOSC_STARTUP_TIME_2     = 0x1, // 61us
    XOSC_STARTUP_TIME_4     = 0x2, // 122us
    XOSC_STARTUP_TIME_8     = 0x3, // 244us
    XOSC_STARTUP_TIME_16    = 0x4, // 488us
    XOSC_STARTUP_TIME_32    = 0x5, // 977us
    XOSC_STARTUP_TIME_64    = 0x6, // 1953us
    XOSC_STARTUP_TIME_128   = 0x7, // 3906us
    XOSC_STARTUP_TIME_256   = 0x8, // 7813us
    XOSC_STARTUP_TIME_512   = 0x9, // 15625us
    XOSC_STARTUP_TIME_1024  = 0xA, // 31250us
    XOSC_STARTUP_TIME_2048  = 0xB, // 62500us
    XOSC_STARTUP_TIME_4096  = 0xC, // 125000us
    XOSC_STARTUP_TIME_8192  = 0xD, // 250000us
    XOSC_STARTUP_TIME_16384 = 0xE, // 500000us
    XOSC_STARTUP_TIME_32768 = 0xF  // 1000000us
} sysctrl_xosc_startup_time;

typedef enum
{
    XOSC32K_STARTUP_TIME_1        = 0x0, // 122us
    XOSC32K_STARTUP_TIME_32       = 0x1, // 1068us
    XOSC32K_STARTUP_TIME_2048     = 0x2, // 62592us
    XOSC32K_STARTUP_TIME_4096     = 0x3, // 125092us
    XOSC32K_STARTUP_TIME_16384    = 0x4, // 500092us
    XOSC32K_STARTUP_TIME_32768    = 0x5, // 1000092us
    XOSC32K_STARTUP_TIME_65536    = 0x6, // 2000092us
    XOSC32K_STARTUP_TIME_131072   = 0x7  // 4000092us
} sysctrl_xosc32k_startup_time;

typedef enum
{
    OSC32K_STARTUP_TIME_3   = 0x0, // 92us
    OSC32K_STARTUP_TIME_4   = 0x1, // 122us
    OSC32K_STARTUP_TIME_6   = 0x2, // 183us
    OSC32K_STARTUP_TIME_10  = 0x3, // 305us
    OSC32K_STARTUP_TIME_18  = 0x4, // 549us
    OSC32K_STARTUP_TIME_34  = 0x5, // 1038us
    OSC32K_STARTUP_TIME_66  = 0x6, // 2014us
    OSC32K_STARTUP_TIME_130 = 0x7  // 3967us
} sysctrl_osc32k_startup_time;

typedef enum
{
    XOSC_GAIN_2MHZ  = 0x0,
    XOSC_GAIN_4MHZ  = 0x1,
    XOSC_GAIN_8MHZ  = 0x2,
    XOSC_GAIN_16MHZ = 0x3,
    XOSC_GAIN_30MHZ = 0x4, // yes it's 30, not 32
} sysctrl_xosc_gain;

typedef enum
{
    OSC8M_PRESCALE_1 = 0x0,
    OSC8M_PRESCALE_2 = 0x1,
    OSC8M_PRESCALE_4 = 0x2,
    OSC8M_PRESCALE_8 = 0x3
} sysctrl_osc8m_prescalar;

void sysctrl_read_serial_number(uint64_t* out_lower_half, uint64_t* out_upper_half);
bool sysctrl_can_enter_standby(void);
void sysctrl_gosc_set_enabled(sysctrl_clock clk, bool enabled);
void sysctrl_gosc_set_on_demand_enabled(sysctrl_clock clk, bool enabled); // oscillator continues to run in low power modes when requested by a peripheral
void sysctrl_gosc_set_run_in_standby_enabled(sysctrl_clock clk, bool enabled); // oscillator continues to run in standby power mode
void sysctrl_gxosc_set_ext_mode(sysctrl_ext_clock clk, sysctrl_ext_mode mode);
void sysctrl_gosc32k_set_32k_mode_enabled(sysctrl_32k_clock clk, bool enabled);
void sysctrl_gosc32k_set_1k_mode_enabled(sysctrl_32k_clock clk, bool enabled);
void sysctrl_gxosc_set_auto_amp_gain_control_enabled(sysctrl_ext_clock clk, bool enabled);
void sysctrl_xosc_set_start_up_time(sysctrl_xosc_startup_time startup_time);
void sysctrl_xosc_set_gain(sysctrl_xosc_gain gain); // no effect if automatic amplitude gain control is enabled
void sysctrl_xosc32k_set_start_up_time(sysctrl_xosc32k_startup_time startup_time);
void sysctrl_xosc32k_enable_write_lock(void);
void sysctrl_osc32k_enable_write_lock(void);
void sysctrl_osculp32k_enable_write_lock(void);
void sysctrl_osc32k_set_start_up_time(sysctrl_osc32k_startup_time startup_time);
void sysctrl_osc8m_set_prescalar(sysctrl_osc8m_prescalar prescalar);
void sysctrl_osc32k_load_factory_calibration(void);
bool sysctrl_osc8m_get_ready_flag(void);
bool sysctrl_osc32k_get_ready_flag(void);
bool sysctrl_xosc_get_ready_flag(void);
bool sysctrl_xosc32k_get_ready_flag(void);


typedef enum
{
    BOD33_CONTINUOUS_MODE = 0x0,
    BOD33_SAMPLING_MODE   = 0x1
} sysctrl_bod33_mode;

typedef enum
{
    BOD33_PRESCALE_2     = 0x0,
    BOD33_PRESCALE_4     = 0x1,
    BOD33_PRESCALE_8     = 0x2,
    BOD33_PRESCALE_16    = 0x3,
    BOD33_PRESCALE_32    = 0x4,
    BOD33_PRESCALE_64    = 0x5,
    BOD33_PRESCALE_128   = 0x6,
    BOD33_PRESCALE_256   = 0x7,
    BOD33_PRESCALE_512   = 0x8,
    BOD33_PRESCALE_1024  = 0x9,
    BOD33_PRESCALE_2048  = 0xA,
    BOD33_PRESCALE_4096  = 0xB,
    BOD33_PRESCALE_8192  = 0xC,
    BOD33_PRESCALE_16384 = 0xD,
    BOD33_PRESCALE_32768 = 0xE,
    BOD33_PRESCALE_65536 = 0xF
} sysctrl_bod33_prescalar;

typedef enum
{
    BOD33_NO_ACTION        = 0x0,
    BOD33_RESET_ACTION     = 0x1,
    BOD33_INTERRUPT_ACTION = 0x2
} sysctrl_bod33_action;

typedef enum
{                    // Typical ratings given below:
    BOD33_6  = 0x06, // VBOD+ with Hysteresis On: 1.715V : VBOD- or VBOD with Hysteresis On or Off: 1.640V
    BOD33_7  = 0x07, // VBOD+ with Hysteresis On: 1.750V : VBOD- or VBOD with Hysteresis On or Off: 1.675V
    BOD33_39 = 0x27, // VBOD+ with Hysteresis On: 2.840V : VBOD- or VBOD with Hysteresis On or Off: 2.770V
    BOD33_48 = 0x30  // VBOD+ with Hysteresis On: 3.200V : VBOD- or VBOD with Hysteresis On or Off: 3.070V
} sysctrl_bod33_level;

void sysctrl_bod33_set_run_in_standby_enabled(bool enabled);
void sysctrl_bod33_set_operation_mode(sysctrl_bod33_mode mode);
void sysctrl_bod33_set_sampling_clock_enabled(bool enabled);
void sysctrl_bod33_set_prescalar(sysctrl_bod33_prescalar prescalar);
void sysctrl_bod33_set_action(sysctrl_bod33_action action);
void sysctrl_bod33_set_level(sysctrl_bod33_level level); // Note: this is loaded at startup from the NVM User Row
void sysctrl_bod33_set_hysteresis_enabled(bool enabled); // Note: this is loaded at startup from the NVM User Row
void sysctrl_bod33_set_enabled(bool enabled); // Note: this is loaded at startup from the NVM User Row
bool sysctrl_bod33_get_synchronizing_flag(void); // if true, BOD33 synchronization is ongoing
bool sysctrl_bod33_get_detection_flag(void); // if true, BOD33 has detected that the I/O power supply is going below the BOD33 reference value
bool sysctrl_bod33_get_ready_flag(void);

// if true, the voltage regulator is in low power and high drive configuration in standby sleep mode.
// if false, the voltage regulator is in low power and low drive configuration in standby sleep mode.
void sysctrl_vreg_set_high_drive_enabled(bool enabled);
void sysctrl_vreg_set_run_in_standby_enabled(bool enabled);
void sysctrl_vref_set_bandgap_output_enabled(bool enabled);
void sysctrl_vref_set_temperature_sensor_enabled(bool enabled);

void sysctrl_dfll48m_set_wait_for_lock_enabled(bool enabled);
void sysctrl_dfll48m_set_coarse_lock_bypass_enabled(bool enabled);
void sysctrl_dfll48m_set_quick_lock_enabled(bool enabled);
void sysctrl_dfll48m_set_chill_cycle_enabled(bool enabled);
void sysctrl_dfll48m_set_usb_clock_recovery_mode_enabled(bool enabled);
void sysctrl_dfll48m_set_lose_lock_after_wake_enabled(bool enabled);
void sysctrl_dfll48m_set_stable_dfll_frequency_enabled(bool enabled); // if enabled, FINE will be fixed after a FINE lock, this is the 'stable' it refers to
void sysctrl_dfll48m_set_closed_loop_mode_enabled(bool enabled);
uint16_t sysctrl_dfll48m_get_closed_loop_multiplication_ratio_difference(void);
void sysctrl_dfll48m_load_open_loop_coarse_factory_calibration(void);
void sysctrl_dfll48m_set_open_loop_coarse_value(uint8_t coarse); // 6 bits
void sysctrl_dfll48m_set_open_loop_fine_value(uint16_t fine); // 10 bits
void sysctrl_dfll48m_set_coarse_max_step(uint8_t cstep); // 6 bits
void sysctrl_dfll48m_set_fine_max_step(uint16_t fstep); // 10 bits
void sysctrl_dfll48m_set_dfll_multiply_factor(uint16_t mul); // 16 bits
uint32_t sysctrl_dfll48m_get_closed_loop_mode_value(void);
bool sysctrl_dfll48m_get_reference_clock_stopped_flag(void);
bool sysctrl_dfll48m_get_coarse_lock_detected_flag(void);
bool sysctrl_dfll48m_get_fine_lock_detected_flag(void);
bool sysctrl_dfll48m_get_out_of_bounds_detected_flag(void);
bool sysctrl_dfll48m_get_ready_flag(void);


typedef enum
{
    FDPLL96M_LOCKTIME_DEFAULT = 0x0,
    FDPLL96M_LOCKTIME_8MS     = 0x4,
    FDPLL96M_LOCKTIME_9MS     = 0x5,
    FDPLL96M_LOCKTIME_10MS    = 0x6,
    FDPLL96M_LOCKTIME_11MS    = 0x7
} sysctrl_fdpll96m_lock_time;

typedef enum
{
    FDPLL96M_REFCLK_XOSC32    = 0x0,
    FDPLL96M_REFCLK_XOSC      = 0x1,
    FDPLL96M_REFCLK_GCLK_DPLL = 0x2
} sysctrl_fdpll96m_reference_clock;

typedef enum
{
    FDPLL96M_FILTER_DEFAULT = 0x0, // default filter
    FDPLL96M_FILTER_LB      = 0x1, // low bandwidth filter
    FDPLL96M_FILTER_HB      = 0x2, // high bandwidth filter
    FDPLL96M_FILTER_HD      = 0x3  // high damping filter
} sysctrl_fdpll96m_filter_mode;

void sysctrl_fdpll96m_set_loop_divider_ratio(uint16_t ldr); // 12 bits
void sysctrl_fdpll96m_set_loop_divider_ratio_fractional_part(uint8_t ldrfrac); // 4 bits
void sysctrl_fdpll96m_set_xosc_division_factor(uint16_t div_factor); // 10 bits
void sysctrl_fdpll96m_set_lock_bypass_enabled(bool enabled);
void sysctrl_fdpll96m_set_lock_time(sysctrl_fdpll96m_lock_time lock_time);
void sysctrl_fdpll96m_set_reference_clock(sysctrl_fdpll96m_reference_clock clk);
void sysctrl_fdpll96m_set_wake_up_fast_enabled(bool enabled);
void sysctrl_fdpll96m_set_low_power_enabled(bool enabled); // reduces power consumption when enabled, but also increases jitter
void sysctrl_fdpll96m_set_filter_mode(sysctrl_fdpll96m_filter_mode mode);
bool sysctrl_fdpll96m_get_reference_clock_divider_enabled_flag(void);
bool sysctrl_fdpll96m_get_dpll_enabled_flag(void);
bool sysctrl_fdpll96m_get_output_clock_ready_flag(void);
bool sysctrl_fdpll96m_get_lock_signal_flag(void);
bool sysctrl_fdpll96m_get_lock_timeout_detected_flag(void);
bool sysctrl_fdpll96m_get_lock_fall_edge_detected_flag(void);
bool sysctrl_fdpll96m_get_lock_rise_edge_detected_flag(void);

#endif /* _DRIVERS_SYSCTRL_H_ */
