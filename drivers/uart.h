#ifndef _DRIVERS_UART_H_
#define _DRIVERS_UART_H_

#include "drivers/defs.h"

// Note: CTRLA, CTRLB, BAUD are protected when USART is enabled

typedef enum
{
    USART_CLK_SRC_EXTERNAL = 0x0,
    USART_CLK_SRC_INTERNAL = 0x1
} usart_clock_source;

typedef enum
{
    USART_ASYNCHRONOUS = 0x0,
    USART_SYNCHRONOUS  = 0x1
} usart_sync_mode;

typedef enum
{
    USART_PINOUT_A = 0x00, // RX: PAD[0], TX: PAD[0], XCK: PAD[1], RTS: NA, CTS: NA
    USART_PINOUT_B = 0x01, // RX: PAD[0], TX: PAD[2], XCK: PAD[3], RTS: NA, CTS: NA
    USART_PINOUT_C = 0x02, // RX: PAD[0], TX: PAD[0], XCK: NA,     RTS: PAD[2], CTS: PAD[3]
    USART_PINOUT_D = 0x10, // RX: PAD[1], TX: PAD[0], XCK: PAD[1], RTS: NA, CTS: NA
    USART_PINOUT_E = 0x11, // RX: PAD[1], TX: PAD[2], XCK: PAD[3], RTS: NA, CTS: NA
    USART_PINOUT_F = 0x12, // RX: PAD[1], TX: PAD[0], XCK: NA,     RTS: PAD[2], CTS: PAD[3]
    USART_PINOUT_G = 0x20, // RX: PAD[2], TX: PAD[0], XCK: PAD[1], RTS: NA, CTS: NA
    USART_PINOUT_H = 0x21, // RX: PAD[2], TX: PAD[2], XCK: PAD[3], RTS: NA, CTS: NA
    USART_PINOUT_I = 0x22, // RX: PAD[2], TX: PAD[0], XCK: NA,     RTS: PAD[2], CTS: PAD[3]
    USART_PINOUT_J = 0x30, // RX: PAD[3], TX: PAD[0], XCK: PAD[1], RTS: NA, CTS: NA
    USART_PINOUT_K = 0x31, // RX: PAD[3], TX: PAD[2], XCK: PAD[3], RTS: NA, CTS: NA
    USART_PINOUT_L = 0x32  // RX: PAD[3], TX: PAD[0], XCK: NA,     RTS: PAD[2], CTS: PAD[3]
} usart_pinout;

typedef enum
{
    USART_CHAR_SIZE_8 = 0x0,
    USART_CHAR_SIZE_9 = 0x1,
    USART_CHAR_SIZE_5 = 0x5,
    USART_CHAR_SIZE_6 = 0x6,
    USART_CHAR_SIZE_7 = 0x7
} usart_character_size;

typedef enum
{
    USART_DATA_ORDER_MSB = 0x0,
    USART_DATA_ORDER_LSB = 0x1
} usart_data_order;

typedef enum
{
    USART_PARITY_EVEN = 0x0,
    USART_PARITY_ODD  = 0x1
} usart_parity_mode;

typedef enum
{
    USART_STOP_BITS_1 = 0x0,
    USART_STOP_BITS_2 = 0x1
} usart_stop_bits;

void usart_set_clock_source(Sercom* usart, usart_clock_source clk_src);
void usart_set_sync_mode(Sercom* usart, usart_sync_mode mode);
void usart_set_pinout(Sercom* usart, usart_pinout pinout);
void usart_set_character_size(Sercom* usart, usart_character_size size);
void usart_set_data_order(Sercom* usart, usart_data_order order);
void usart_set_parity_mode(Sercom* usart, usart_parity_mode mode);
void usart_set_stop_bits(Sercom* usart, usart_stop_bits bits);
void usart_set_baudrate(Sercom* usart, uint32_t sercom_clk_rate, uint32_t desired_baudrate); // for internal clock mode only
uint32_t usart_get_baudrate(Sercom* usart, uint32_t sercom_clk_rate); // for internal clock mode only, returns closest achievable baudrate
void usart_set_transceiver_enabled(Sercom* usart, bool transmitter_enabled, bool receiver_enabled);
void usart_set_enabled(Sercom* usart, bool enabled);
void usart_swreset(Sercom* usart);

typedef enum
{
    USART_TX_RISE_RX_FALL = 0x0,
    USART_TX_FALL_RX_RISE = 0x1
} usart_clock_polarity;

typedef enum
{
    USART_FORMAT_USART_FRAME             = 0x0,
    USART_FORMAT_USART_FRAME_WITH_PARITY = 0x1,
    USART_FORMAT_AUTO_BAUD               = 0x4,
    USART_FORMAT_AUTO_BAUD_WITH_PARITY   = 0x5
} usart_frame_format;

typedef enum
{
    USART_SAMPLE_RATE_16X_ARITHMETIC = 0x0,
    USART_SAMPLE_RATE_16X_FRACTIONAL = 0x1,
    USART_SAMPLE_RATE_8X_ARITHMETIC  = 0x2,
    USART_SAMPLE_RATE_8X_FRACTIONAL  = 0x3,
    USART_SAMPLE_RATE_3X_ARITHMETIC  = 0x4
} usart_oversampling_rate;

typedef enum
{
    USART_SAMPLE_RATE_ADJUSTMENT_MODE_0 = 0x0, // 16x Over-sampling (CTRLA.SAMPR=0 or 1): 7-8-9    , 8x Over-sampling (CTRLA.SAMPR=2 or 3): 3-4-5
    USART_SAMPLE_RATE_ADJUSTMENT_MODE_1 = 0x1, // 16x Over-sampling (CTRLA.SAMPR=0 or 1): 9-10-11  , 8x Over-sampling (CTRLA.SAMPR=2 or 3): 4-5-6
    USART_SAMPLE_RATE_ADJUSTMENT_MODE_2 = 0x2, // 16x Over-sampling (CTRLA.SAMPR=0 or 1): 11-12-13 , 8x Over-sampling (CTRLA.SAMPR=2 or 3): 5-6-7
    USART_SAMPLE_RATE_ADJUSTMENT_MODE_3 = 0x3  // 16x Over-sampling (CTRLA.SAMPR=0 or 1): 13-14-15 , 8x Over-sampling (CTRLA.SAMPR=2 or 3): 6-7-8
} usart_oversampling_adjustment_mode;

void usart_set_clock_polarity(Sercom* usart, usart_clock_polarity polarity); // for synchronous mode only
void usart_set_frame_format(Sercom* usart, usart_frame_format format);
void usart_set_oversampling_rate(Sercom* usart, usart_oversampling_rate rate);
void usart_set_oversampling_adjustment_mode(Sercom* usart, usart_oversampling_adjustment_mode mode);

void usart_set_immediate_buffer_overflow_notification_enabled(Sercom* usart, bool enabled);
void usart_set_run_in_standby_enabled(Sercom* usart, bool enabled);

void usart_set_irda_encoding_enabled(Sercom* usart, bool enabled);
void usart_set_irda_receive_pulse_length(Sercom* usart, uint8_t pulse);

void usart_set_collision_detection_enabled(Sercom* usart, bool enabled);
void usart_set_start_of_frame_detection_enabled(Sercom* usart, bool enabled);

typedef enum
{
    USART_ERROR_NONE                          = 0x0,
    USART_ERROR_INVALID_ARGUMENT              = 0x1,
    USART_ERROR_TIMEOUT                       = 0x2,
    USART_ERROR_BUSY                          = 0x3,
    USART_ERROR_FRAME_ERROR                   = 0x4,
    USART_ERROR_PARITY_ERROR                  = 0x5,
    USART_ERROR_COLLISION_ERROR               = 0x6,
    USART_ERROR_OVERFLOW_ERROR                = 0x7,
    USART_ERROR_INCONSISTENT_SYNC_FIELD_ERROR = 0x8
} usart_error;

usart_error usart_sync_write(Sercom* usart, uint16_t* in_data, uint32_t data_length);
usart_error usart_sync_read(Sercom* usart, uint16_t* out_data, uint32_t data_length, uint32_t timeout_ms); // use 0xFFFFFFFF to equal forever

usart_error usart_async_write(Sercom* usart, uint16_t* in_data, uint32_t data_length);
usart_error usart_async_read(Sercom* usart, uint16_t* out_data, uint32_t data_length);

void usart_cancel_async_write(Sercom* usart);
void usart_cancel_async_read(Sercom* usart, bool give_read_data); // if true, receive complete will be called if any data has been received and buffered

void usart_interrupt_handler(Sercom* usart);
// example usage: void SERCOM0_Handler() { usart_interrupt_handler(SERCOM0); }

extern void (*usart_error_callback)(uint8_t sercom_index, usart_error error);
extern void (*usart_receive_started_callback)(uint8_t sercom_index);
extern void (*usart_cts_changed_callback)(uint8_t sercom_index, bool cts_state);
extern void (*usart_break_received_callback)(uint8_t sercom_index);
extern void (*usart_receive_complete_callback)(uint8_t sercom_index, uint16_t* received_data, uint32_t data_length, bool nine_bit_mode);
extern void (*usart_receive_character_callback)(uint8_t sercom_index, uint16_t received_char, bool nine_bit_mode);
extern void (*usart_transmit_complete_callback)(uint8_t sercom_index, uint16_t* sent_data, uint32_t data_length, bool nine_bit_mode);

#endif /* _DRIVERS_UART_H_ */
