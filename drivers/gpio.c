#include "drivers/gpio.h"

#define EXTRACT_PORT(x)  ((x & 0x20) >> 5)
#define EXTRACT_PIN(x)   (x & 0x1F)

void gpio_set_io_mode(gpio_pin pin, gpio_io_mode mode)
{
    uint32_t group_index = EXTRACT_PORT(pin);
    uint32_t pin_index = EXTRACT_PIN(pin);

    if(mode == GPIO_OUTPUT)
    {
        PORT->Group[group_index].DIR.reg |= 1 << pin_index;
        PORT->Group[group_index].PINCFG[pin_index].bit.INEN = 0;
    }
    else
    {
        PORT->Group[group_index].PINCFG[pin_index].bit.INEN = 1;
        PORT->Group[group_index].DIR.reg &= ~(1 << pin_index);
    }
}

void gpio_set_input_pull_mode(gpio_pin pin, gpio_io_pull_mode mode)
{
    uint32_t group_index = EXTRACT_PORT(pin);
    uint32_t pin_index = EXTRACT_PIN(pin);

    if(mode == GPIO_PULL_NONE)
    {
        PORT->Group[group_index].PINCFG[pin_index].bit.PULLEN = 0;
    }
    else
    {
        PORT->Group[group_index].PINCFG[pin_index].bit.PULLEN = 1;
        if(mode == GPIO_PULL_UP)
        {
            PORT->Group[group_index].OUT.reg |= 1 << pin_index;
        }
        else
        {
            PORT->Group[group_index].OUT.reg &= ~(1 << pin_index);
        }
    }
}

void gpio_set_input_sampling_mode(gpio_pin pin, gpio_io_input_sampling_mode mode)
{
    uint32_t group_index = EXTRACT_PORT(pin);
    uint32_t pin_index = EXTRACT_PIN(pin);

    if(mode == GPIO_INPUT_SAMPLING_CONTINUOUS)
    {
        PORT->Group[group_index].CTRL.reg |= 1 << pin_index;
    }
    else
    {
        PORT->Group[group_index].CTRL.reg &= ~(1 << pin_index);
    }
}

void gpio_set_alternate_function_enabled(gpio_pin pin, bool enabled)
{
    uint32_t group_index = EXTRACT_PORT(pin);
    uint32_t pin_index = EXTRACT_PIN(pin);

    PORT->Group[group_index].PINCFG[pin_index].bit.PMUXEN = enabled;
}

void gpio_set_alternate_function(gpio_pin pin, gpio_io_alternate_function alt_func)
{
    uint32_t group_index = EXTRACT_PORT(pin);
    uint32_t pin_index = EXTRACT_PIN(pin);
    uint32_t pmux_index = pin >> 1;

    if(pin_index % 2)
    {
        PORT->Group[group_index].PMUX[pmux_index].bit.PMUXO = alt_func;
    }
    else
    {
        PORT->Group[group_index].PMUX[pmux_index].bit.PMUXE = alt_func;
    }
}

void gpio_set_drive_strength(gpio_pin pin, gpio_io_drive_strength strength)
{
    uint32_t group_index = EXTRACT_PORT(pin);
    uint32_t pin_index = EXTRACT_PIN(pin);

    PORT->Group[group_index].PINCFG[pin_index].bit.DRVSTR = strength;
}

void gpio_set_level(gpio_pin pin, gpio_io_state state)
{
    uint32_t group_index = EXTRACT_PORT(pin);
    uint32_t pin_index = EXTRACT_PIN(pin);

    if(state == GPIO_HIGH)
    {
        PORT->Group[group_index].OUT.reg |= 1 << pin_index;
    }
    else
    {
        PORT->Group[group_index].OUT.reg &= ~(1 << pin_index);
    }
}

gpio_io_state gpio_read_level(gpio_pin pin)
{
    uint32_t group_index = EXTRACT_PORT(pin);
    uint32_t pin_index = EXTRACT_PIN(pin);

    if(PORT->Group[group_index].IN.reg & (1 << pin_index))
    {
        return GPIO_HIGH;
    }

    return GPIO_LOW;
}
