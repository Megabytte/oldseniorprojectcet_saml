#ifndef _DRIVERS_PM_H_
#define _DRIVERS_PM_H_

#include "drivers/defs.h"

typedef enum
{
    PM_PRESCALE_1    = 0x0,
    PM_PRESCALE_2    = 0x1,
    PM_PRESCALE_4    = 0x2,
    PM_PRESCALE_8    = 0x3,
    PM_PRESCALE_16   = 0x4,
    PM_PRESCALE_32   = 0x5,
    PM_PRESCALE_64   = 0x6,
    PM_PRESCALE_128  = 0x7
} pm_clock_prescalar;

typedef enum
{
    PM_CLK_CPU, // the AHB clock is always equal to the CPU clock
    PM_CLK_APB_A,
    PM_CLK_APB_B,
    PM_CLK_APB_C
} pm_clk;

typedef enum
{
    RESET_UNKNOWN   = 0x0,
    RESET_POR       = 0x1,
    RESET_BOD12     = 0x2,
    RESET_BOD33     = 0x4,
    RESET_EXTERNAL  = 0x10,
    RESET_WDT       = 0x20,
    RESET_CPU       = 0x40
} pm_reset_source;

typedef enum
{
    MODE_IDLE0   = 0x0,
    MODE_IDLE1   = 0x1,
    MODE_IDLE2   = 0x2,
    MODE_STANDBY = 0x3,
    MODE_ACTIVE  = 0x4
} pm_mode;

typedef enum
{
    AHB_HPB0     = 0x00,
    AHB_HPB1     = 0x01,
    AHB_HPB2     = 0x02,
    AHB_DSU      = 0x03,
    AHB_NVMCTRL  = 0x04,
    AHB_DMAC     = 0x05,
    AHB_USB      = 0x06,
    APBA_PAC0    = 0x40,
    APBA_PM      = 0x41,
    APBA_SYSCTRL = 0x42,
    APBA_GCLK    = 0x43,
    APBA_WDT     = 0x44,
    APBA_RTC     = 0x45,
    APBA_EIC     = 0x46,
    APBB_PAC1    = 0x80,
    APBB_DSU     = 0x81,
    APBB_NVMCTRL = 0x82,
    APBB_PORT    = 0x83,
    APBB_DMAC    = 0x84,
    APBB_USB     = 0x85,
    APBC_PAC2    = 0xC0,
    APBC_EVSYS   = 0xC1,
    APBC_SERCOM0 = 0xC2,
    APBC_SERCOM1 = 0xC3,
    APBC_SERCOM2 = 0xC4,
    APBC_SERCOM3 = 0xC5,
    APBC_SERCOM4 = 0xC6,
    APBC_SERCOM5 = 0xC7,
    APBC_TCC0    = 0xC8,
    APBC_TCC1    = 0xC9,
    APBC_TCC2    = 0xCA,
    APBC_TC3     = 0xCB,
    APBC_TC4     = 0xCC,
    APBC_TC5     = 0xCD,
    APBC_TC6     = 0xCE,
    APBC_TC7     = 0xCF,
    APBC_ADC     = 0xD0,
    APBC_AC      = 0xD1,
    APBC_DAC     = 0xD2,
    APBC_PTC     = 0xD3,
    APBC_I2S     = 0xD4,
    APBC_AC1     = 0xD5,
    APBC_TCC3    = 0xD8
} pm_peripheral_clk;

void pm_reset_mcu(void);
void pm_enter_mode(pm_mode mode, bool return_to_sleep_after_interrupt);
pm_reset_source pm_get_reset_source(void);
void pm_set_prescalar(pm_clk clk, pm_clock_prescalar prescalar); // Remember, CPU must be faster than other clocks, and be careful not to exceed each domain's max speed
void pm_set_peripheral_clk_enabled(pm_peripheral_clk pclk, bool enabled);

#endif /* _DRIVERS_PM_H_ */
