#ifndef _DRIVERS_GPIO_H_
#define _DRIVERS_GPIO_H_

#include "drivers/defs.h"

typedef enum
{
    GPIO_INPUT = 0x0,
    GPIO_OUTPUT = 0x01
} gpio_io_mode;

typedef enum
{
    GPIO_PULL_NONE = 0x0,
    GPIO_PULL_DOWN = 0x1,
    GPIO_PULL_UP = 0x2
} gpio_io_pull_mode;

typedef enum
{
    GPIO_LOW = 0x0,
    GPIO_HIGH = 0x1
} gpio_io_state;

typedef enum
{
    GPIO_DRIVE_NORMAL = 0x0,
    GPIO_DRIVE_HIGH = 0x1
} gpio_io_drive_strength;

typedef enum
{
    GPIO_INPUT_SAMPLING_ON_DEMAND = 0x0,
    GPIO_INPUT_SAMPLING_CONTINUOUS = 0x1
} gpio_io_input_sampling_mode;

typedef enum
{
    GPIO_AF_A = 0x0,
    GPIO_AF_B = 0x1,
    GPIO_AF_C = 0x2,
    GPIO_AF_D = 0x3,
    GPIO_AF_E = 0x4,
    GPIO_AF_F = 0x5,
    GPIO_AF_G = 0x6,
    GPIO_AF_H = 0x7,
    GPIO_AF_I = 0x8
} gpio_io_alternate_function;

typedef enum
{
    PA00 = 0x00,
    PA01 = 0x01,
    PA02 = 0x02,
    PA03 = 0x03,
    PA04 = 0x04,
    PA05 = 0x05,
    PA06 = 0x06,
    PA07 = 0x07,
    PA08 = 0x08,
    PA09 = 0x09,
    PA10 = 0x0A,
    PA11 = 0x0B,
    PA12 = 0x0C,
    PA13 = 0x0D,
    PA14 = 0x0E,
    PA15 = 0x0F,
    PA16 = 0x10,
    PA17 = 0x11,
    PA18 = 0x12,
    PA19 = 0x13,
    PA20 = 0x14,
    PA21 = 0x15,
    PA22 = 0x16,
    PA23 = 0x17,
    PA24 = 0x18,
    PA25 = 0x19,
    PA27 = 0x1B,
    PA28 = 0x1C,
    PA30 = 0x1E,
    PA31 = 0x1F,
    PB00 = 0x20,
    PB01 = 0x21,
    PB02 = 0x22,
    PB03 = 0x23,
    PB04 = 0x24,
    PB05 = 0x25,
    PB06 = 0x26,
    PB07 = 0x27,
    PB08 = 0x28,
    PB09 = 0x29,
    PB10 = 0x2A,
    PB11 = 0x2B,
    PB12 = 0x2C,
    PB13 = 0x2D,
    PB14 = 0x2E,
    PB15 = 0x2F,
    PB16 = 0x30,
    PB17 = 0x31,
    PB22 = 0x36,
    PB23 = 0x37,
    PB30 = 0x3E,
    PB31 = 0x3F
} gpio_pin;

void gpio_set_io_mode(gpio_pin pin, gpio_io_mode mode);
void gpio_set_input_pull_mode(gpio_pin pin, gpio_io_pull_mode mode);
void gpio_set_input_sampling_mode(gpio_pin pin, gpio_io_input_sampling_mode mode);
void gpio_set_alternate_function_enabled(gpio_pin pin, bool enabled);
void gpio_set_alternate_function(gpio_pin pin, gpio_io_alternate_function alt_func);
void gpio_set_drive_strength(gpio_pin pin, gpio_io_drive_strength strength);
void gpio_set_level(gpio_pin pin, gpio_io_state state);
gpio_io_state gpio_read_level(gpio_pin pin);

#endif /* _DRIVERS_GPIO_H_ */
