#include "drivers/spi.h"

void spi_set_op_mode(Sercom* spi, spi_op_mode mode)
{
    spi->SPI.CTRLA.bit.MODE = mode;
}

void spi_set_clock_mode(Sercom* spi, spi_clock_mode mode)
{
    spi->SPI.CTRLA.bit.CPOL = (bool)(mode & 2);
    spi->SPI.CTRLA.bit.CPHA = (bool)(mode & 1);
}

void spi_set_data_order(Sercom* spi, spi_data_order order)
{
    spi->SPI.CTRLA.bit.DORD = order;
}

void spi_set_pinout(Sercom* spi, spi_pinout pinout)
{
    spi->SPI.CTRLA.bit.DIPO = (pinout & 0xF0) >> 4;
    spi->SPI.CTRLA.bit.DOPO = pinout & 0x0F;
}

void spi_set_frame_format(Sercom* spi, spi_frame_format format)
{
    spi->SPI.CTRLA.bit.FORM = format;
}

void spi_set_character_size(Sercom* spi, spi_character_size ch_size)
{
    spi->SPI.CTRLB.bit.CHSIZE = ch_size;
}

void spi_slave_set_address_mode(Sercom* spi, spi_address_mode mode)
{
    spi->SPI.CTRLB.bit.AMODE = mode;
}

void spi_slave_set_address_value(Sercom* spi, uint8_t primary_address, uint8_t address_mask_or_secondary_address_or_unused)
{
    spi->SPI.ADDR.bit.ADDR = primary_address;
    spi->SPI.ADDR.bit.ADDRMASK = address_mask_or_secondary_address_or_unused;
}

void spi_slave_set_select_low_detection_enabled(Sercom* spi, bool enabled)
{
    spi->SPI.CTRLB.bit.SSDE = enabled;
}

void spi_slave_set_data_preload_enabled(Sercom* spi, bool enabled)
{
    spi->SPI.CTRLB.bit.PLOADEN = enabled;
}

void spi_master_set_baudrate(Sercom* spi, uint32_t sercom_clk_rate, uint32_t desired_baudrate)
{
    spi->SPI.BAUD.reg = (uint16_t)(65536 * (1 - 16 * ((float)desired_baudrate / sercom_clk_rate)));
}

uint32_t spi_master_get_baudrate(Sercom* spi, uint32_t sercom_clk_rate)
{
    return (uint32_t)((float)sercom_clk_rate / 16) * (1 - ((float)spi->SPI.BAUD.reg / 65536));
}

void spi_master_set_hardware_slave_select_enabled(Sercom* spi, bool enabled)
{
    spi->SPI.CTRLB.bit.MSSEN = enabled;
}

void spi_set_receiver_enabled(Sercom* spi, bool enabled)
{
    spi->SPI.CTRLB.bit.RXEN = enabled;
    while(spi->SPI.SYNCBUSY.bit.CTRLB);
}

void spi_set_immediate_buffer_overflow_notification_enabled(Sercom* spi, bool enabled)
{
    spi->SPI.CTRLA.bit.IBON = enabled;
}

void spi_set_run_in_standby_enabled(Sercom* spi, bool enabled)
{
    spi->SPI.CTRLA.bit.RUNSTDBY = enabled;
}

void spi_enable_interrupt(Sercom* spi, spi_interrupt interrupt)
{
    spi->SPI.INTENSET.reg = interrupt;
}

void spi_disable_interrupt(Sercom* spi, spi_interrupt interrupt)
{
    spi->SPI.INTENCLR.reg = interrupt;
}

void spi_set_enabled(Sercom* spi, bool enabled)
{
    spi->SPI.CTRLA.bit.ENABLE = enabled;
    while(spi->SPI.SYNCBUSY.bit.ENABLE);
}

void spi_swreset(Sercom* spi)
{
    spi->SPI.CTRLA.bit.SWRST = 1;
    while(spi->SPI.SYNCBUSY.bit.SWRST || spi->SPI.CTRLA.bit.SWRST);
}

void spi_master_sync_transfer(Sercom* spi, uint16_t* data_out, uint16_t* data_in, uint32_t data_length)
{
    bool nine_bit_mode = spi->SPI.CTRLB.bit.CHSIZE;
    uint8_t* eight_tx = (uint8_t*)data_out;
    uint8_t* eight_rx = (uint8_t*)data_in;
    uint16_t* nine_tx = data_out;
    uint16_t* nine_rx = data_in;
    for(uint32_t i = 0; i < data_length; i++)
    {
        while(!spi->SPI.INTFLAG.bit.DRE);

        if(nine_bit_mode)
        {
            spi->SPI.DATA.reg = *nine_tx;
        }
        else
        {
            spi->SPI.DATA.reg = *eight_tx;
        }

        while(!spi->SPI.INTFLAG.bit.RXC);

        if(nine_bit_mode)
        {
            *nine_rx = spi->SPI.DATA.reg;
        }
        else
        {
            *eight_rx = spi->SPI.DATA.reg;
        }

        eight_rx++;
        eight_tx++;
        nine_rx++;
        nine_tx++;
    }
}

void spi_master_async_transfer(Sercom* spi, uint16_t* data_out, uint16_t* data_in, uint32_t data_length)
{

}

void spi_master_interrupt_handler(Sercom* spi)
{

}

void spi_slave_interrupt_handler(Sercom* spi)
{

}
