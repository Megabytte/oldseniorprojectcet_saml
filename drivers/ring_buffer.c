#include "ring_buffer.h"

#include <string.h>

const char* generic_ring_buffer_error_str[3] =
{
    "GRBE_NONE",
    "GRBE_NOT_WRITTEN",
    "GRBE_OVER_WRITTEN"
};

void generic_ring_buffer_init(generic_ring_buffer* rbuf, void* buffer, uint32_t element_size, uint32_t buffer_size)
{
    rbuf->buffer = (uint8_t*)buffer;
    rbuf->size = buffer_size;
    rbuf->element_size = element_size;
    rbuf->head = rbuf->tail = rbuf->buffer;
    rbuf->count = 0;
}

void generic_ring_buffer_reset(generic_ring_buffer* rbuf)
{
    rbuf->head = rbuf->tail = rbuf->buffer;
    rbuf->count = 0;
}

generic_ring_buffer_error generic_ring_buffer_put(generic_ring_buffer* rbuf, void* data, bool overwrite)
{
    if(rbuf->count == rbuf->size)
    {
        // Head Caught Tail
        if(overwrite)
        {
            memcpy(rbuf->head, data, rbuf->element_size);
            // ignore rbuf->count, we lost an element when we gained one
            rbuf->head += rbuf->element_size;
            rbuf->tail += rbuf->element_size;
            if(rbuf->head >= rbuf->buffer + (rbuf->element_size * rbuf->size))
            {
                rbuf->head = rbuf->buffer;
            }
            if(rbuf->tail >= rbuf->buffer + (rbuf->element_size * rbuf->size))
            {
                rbuf->tail = rbuf->buffer;
            }
            
            return GRBE_OVER_WRITTEN;
        }
        
        return GRBE_NOT_WRITTEN;
    }
    
    memcpy(rbuf->head, data, rbuf->element_size);
    rbuf->head += rbuf->element_size;
    if(rbuf->head >= rbuf->buffer + (rbuf->element_size * rbuf->size))
    {
        rbuf->head = rbuf->buffer;
    }
    rbuf->count++;
    
    return GRBE_NONE;
}

generic_ring_buffer_error generic_ring_buffer_get(generic_ring_buffer* rbuf, void* data)
{
    if(rbuf->count != 0)
    {
        rbuf->count--;
        memcpy(data, rbuf->tail, rbuf->element_size);
        rbuf->tail += rbuf->element_size;
        if(rbuf->tail >= rbuf->buffer + (rbuf->element_size * rbuf->size))
        {
            rbuf->tail = rbuf->buffer;
        }

        return GRBE_NONE;
    }
    
    return GRBE_NOT_WRITTEN;
}

generic_ring_buffer_error generic_ring_buffer_advance_tail(generic_ring_buffer* rbuf)
{
    if(rbuf->count != 0)
    {
        rbuf->count--;
        rbuf->tail += rbuf->element_size;
        if(rbuf->tail >= rbuf->buffer + (rbuf->element_size * rbuf->size))
        {
            rbuf->tail = rbuf->buffer;
        }

        return GRBE_NONE;
    }
    
    return GRBE_NOT_WRITTEN;
}

generic_ring_buffer_error generic_ring_buffer_peek(generic_ring_buffer* rbuf, void* data)
{
    if(rbuf->count != 0)
    {
        memcpy(data, rbuf->tail, rbuf->element_size);
        
        return GRBE_NONE;
    }
    
    return GRBE_NOT_WRITTEN;
}

bool generic_ring_buffer_empty(generic_ring_buffer* rbuf)
{
    return rbuf->count == 0;
}

bool generic_ring_buffer_full(generic_ring_buffer* rbuf)
{
    return rbuf->count == rbuf->size;
}

uint32_t generic_ring_get_count(generic_ring_buffer* rbuf)
{
    return rbuf->count; 
}
