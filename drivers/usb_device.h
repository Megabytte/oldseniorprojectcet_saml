#ifndef _DRIVERS_USB_H_
#define _DRIVERS_USB_H_

#include "drivers/defs.h"

/* SAMD21 USB APIs
  - General Management (Endpoints / Descriptors / RAM)
  - Communication Device Class (CDC)
  - [Future] Composite Devices
  - [Future] Human Interface Device (HID) [Mouse / Keyboard / Joystick]
  - [Future] Mass Storage Device (MSD)
  - [Future?] Audio (Microphone/Speaker) Device
  - [Future?] Media Control Device
  - ???
*/

/* USB Notes
    - Host initiates all transactions
    - USB traffic is LSB first
    - USB descriptors are 12-bytes
    - 8 Endpoint Addresses (Each with 1 Input & 1 Output) for a total of 16 endpoints
    - 4 Transfer Types: Control, Interrupt, Bulk, or Isochronous
    - Need a VID/PID Pair
    - Descriptor Asks for 1-5 power units (100mA) [default=1] and then cannot draw more than that
    - Endpoint 0 is used for device control and status requests during enumeration (and while device operates on the bus)
    - Pipes are what data is transferred through (Logical connection between host and endpoints)
        - Stream Pipe: [Bulk, Isochronous, Interrupt] Any data can be transceived [Host or Device Controlled]
        - Message Pipe: [Control] Special messages only [Host Controlled]
    - Errors seem to be handled as thus:
        - No Error: send ACK
        - Data Corrupt: Ignore packet
        - Temporarily Busy: send NAK
        - Fatal Internal Error: send STALL
    - ??
*/

/* USB Transaction / Packet Structure
    - (1) Token Packet [Header] (Tells device address, if read/write, and what endpoint it is for)
        - Sync [8 bits] (Gets clocks running in sync, and last 2 bits tell where PID field starts)
        - PID  [8 bits, See PID Chart] (Repeated in complement form for a total of 8 bits)
        - ADDR [7 bits] (Formal address 0 is not valid as any unassigned device responds to address 0)
        - ENDP [4 bits] (Specifies the target endpoint)
        - CRC5 [5 bits] (5 bit Cyclic Redundancy Check to ensure data is valid)
        - EOP [3 bits] (2 bits of Single Ended Zero (SE0) and a "J" for 1 bit)
    - (2) Optional Data Packet [Payload] (DATA must be sent in multiples of bytes)
        - Sync
        - PID
        - DATA (Low-Speed Max: 8 bytes, Full-Speed Max: 1023 bytes, High-Speed Max: 1024 bytes)
        - CRC16 (16 bit CRC)
        - EOP
    - (3) Status Packet [ACK/Error Correction] (Tells host if success, stalled, or not available)
        - Sync
        - PID
        - EOP

    - Start of Frame Packet (SOF)
        - Sync
        - PID
        - Frame Number [11 bits] (Sent every 1ms +- 500ns on a full-speed bus, 125 us +- 0.0625 us on high-speed bus)
        - CRC5
        - EOP
*/

/* USB PID Chart
    Token Group:
        0001-1110: OUT [Host sends data]
        1001-0110: IN [Host wants data]
        0101-1010: SOF
        1101-0010: SETUP
    Data Group:
        0011-1100: DATA0
        1011-0100: DATA1
        0111-1000: DATA2 [High Speed Only]
        1111-0000: MDATA [High Speed Only]
    Handshake Group:
        0010-1101: ACK Handshake [Packet successfully received]
        1010-0101: NAK Handshake [Temporarily cannot send/receive data] (or no data if using interrupt mode)
        1110-0001: STALL Handshake [Need help from host] (NAK=wait a moment, STALL=failed and dead)
        0110-1001: NYET (No Response Yet)
    Special Group:
        1100-0011: PREamble
        1100-0011: ERR
        1000-0111: Split
        0100-1011: Ping
*/

/* USB Endpoint Types
    - Bus Time is divided with priority:
        - 1.) Isochronous/Interrupt (up to 90% of bus time)
        - 2.) Control (uses leftover 10%)
        - 3.) Bulk (uses the leftover time after Control has been allocated time)
    - Control Transfer: 8 bytes for low-speed, 64 bytes for full-speed, 8/16/32/64 bytes for high-speed
        - Setup Stage:
            - SETUP Token Packet
            - DATA0 Data Packet
            - ACK Packet or No Response (Illegal to send NAK or STALL)
        - Optional Data Stage: 1 or more mixed IN/OUT transfers
            - IN: [] denotes device action
                - IN, [DATAx], ACK
                - IN, [STALL]
                - IN, [NAK]
                - IN, [Ignore due to corruption]
            - OUT: [] denotes device action
                - OUT, DATAx, [ACK/STALL/NAK/Ignore due to corruption]
        - Status Stage: reports the status of the overall request
            - IN: if host sent IN tokens during Data Stage ([] denotes device action)
                - OUT, DATA0 Zero Length, [ACK/STALL/NACK]
            - OUT: if host sent OUT tokens during Data Stage ([] denotes device action)
                - IN, [DATA0 Zero Length/STALL/NAK], ACK
    - Interrupt Transfer: (Guaranteed Latency, Stream Pipe - Unidirectional, Error detection and next period retry)
        - Max data payloads (L: 8 bytes, F: 64 bytes, H: 1024 bytes)
        - Descriptor defines how often the host will poll the interrupt endpoint
        - IN, [DATAx/STALL=Halt/NAK=No Interrupt Pending], ACK
        - OUT, DATAx, [ACK=Success/NAK=Failure/STALL=Halt]
    - Isochronous Transfer: [Full/High Speed Devices Only] (Error detection via CRC, but no retry or guarantee of delivery)
        - Guaranteed access to USB bandwidth
        - Bounded latency
        - Stream Pipe - Unidirectional
        - No data toggling
        - IN, [DATAx]
        - OUT, DATAx
        - Notice the lack of a handshaking stage!
    - Bulk Transfer: [Full/High Speed Devices Only] (Used to transfer large burst-y data, Error detection via CRC with guarantee of delivery)
        - No guarantee of bandwidth or minimum latency
        - Stream Pipe - Unidirectional
        - Max Full-Speed Endpoint Size: 8/16/32/64 bytes, High-Speed: Up to 512 bytes
        - If data payload falls short of maximum size, data does NOT need to be padded with 0s
        - IN, [DATAx/STALL/NAK], ACK
        - OUT, DATAx, [ACK/NAK/STALL]
        - A bulk transfer is considered complete when:
            - it has transferred the exact amount of data requested
            - transferred a packet less than the maximum endpoint size
            - transferred a zero-length packet
*/

/* Enumeration Process
    - 1st Transaction
        - SETUP Token Packet (host wants to read endpoint 0 of the device)
        - DATA0 Packet (Data0 contains the 8-byte 'read descriptor command')
        - ACK Handshake [Device ACK]
    - 2nd/3rd Transaction
        - IN Token Packet
        - DATA1 Packet (First 8 bytes of device descriptor)
        - ACK Handshake [Host ACK]
        - IN Token Packet
        - DATA0 Packet (Last 4 bytes + padding of device descriptor)
        - ACK Handshake [Host ACK]
    - 4th Transaction (Occurs if the host enumeration was successful)
        - OUT Packet
        - DATA1 Packet [Zero Length Packet]
        - ACK Handshake [Device ACK, this ACK is represents the device's view on the entire enumeration]
*/

/* USB Device Descriptors
    - Note: Endpoint 0 is always assumed to be a control endpoint, and never has a descriptor!
    - Device Descriptor (# of Configuration Descriptors)
        - Configuration Descriptors (power options, # of Interface Descriptors)
            - Interface Descriptors (alternate settings, # of Endpoint Descriptors)
                - Endpoint Descriptor (type of transfer, direction, polling interval, max packet size)

    All Descriptor Compositions:
        - Length: [1 byte / number] Size of Descriptor in bytes
        - Description Type: [1 byte / constant] Defines what type of descriptor is being defined
        - Data: [n bytes] Start of parameters for descriptor

    Device Descriptor Composition (only 1 allowed, and represents the entire device):
        - Length: [1 byte / number] Size of Descriptor in bytes (18 bytes)
        - Description Type: [1 byte / constant] Device Descriptor (0x01)
        - Spec: [2 bytes / BCD] (Format: 0xJJMN, Example: 0x0210 = USB Specification 2.1.0)
        - Device Class: [1 byte / Class Code] Usually 0 unless device is highly-specific in function *used by OS to find driver
            - If 0x00, each interface specifies its own class code
            - If 0xFF, the class code is vendor specified
            - Else, USB Org defined class code
        - Device Sub-Class: [1 byte / Sub-Class Code] Subclass Code (Assigned by USB Org) *used by OS to find driver
        - Device Protocol: [1 byte / Protocol Code] Protocol Code (Assigned by USB Org) *used by OS to find driver
        - Max Packet Size: [1 byte / number] Maximum Packet Size for Zero Endpoint, (8/16/32/64)
        - Vendor ID: [2 bytes / ID] Vendor ID (Assigned by USB Org)
        - Product ID: [2 bytes / ID] Product ID (Assigned by Manufacturer)
        - Device Revision: [2 bytes / BCD] (Format: 0xJJMN, Example: 0x1024) = Manufacturer Device Version 10.2.4
        - Manufacturer Index [1 byte / Index] Index of Manufacturer String Descriptor (not required and 0x00 if none exists)
        - Product Index [1 byte / Index] Index of Product String Descriptor (not required and 0x00 if none exists)
        - Serial Number	Index [1 byte / Index] Index of Serial Number String Descriptor (not required and 0x00 if none exists)
        - Number of Configurations [1 byte / number] Number of Configurations provided by this device

    Configuration Descriptor Composition:
        - Length: [1 byte / number] Size of Descriptor in bytes (9 bytes)
        - Description Type: [1 byte / constant] Configuration Descriptor (0x02)
        - Total Length: [2 bytes / number] Total length of this descriptor + all interface descriptors defined under this configuration
        - Number of Interfaces: [1 byte / number] Number of Interfaces provided by this Configuration
        - Configuration Value: [1 byte / number] Value to use as an argument to select this configuration
        - Configuration Index: [1 byte / Index] Index of Configuration String Descriptor
        - Attribute Bitmap: [1 byte / bitfield] MSB 1AB00000 LSB, If A=1 then device is Self-Powered, If B=1 then device can Remote-Wakeup the host
        - Max Power: [1 byte / 2mA] Maximum Power Consumption in 2 mA units (Max Value of 0xFA -> 2mA * 0xFA = 500mA)

    Interface Descriptor Composition:
        - Length: [1 byte / number] Size of Descriptor in bytes (9 bytes)
        - Description Type: [1 byte / constant] Interface Descriptor (0x04)
        - Interface Number: [1 byte / number] Number of Interface (0 based, and incremented once for each new interface descriptor)
        - Alternate Setting: [1 byte / number] Value used to select alternate setting
        - Number of Endpoints: [1 byte / number] Number of Endpoints provided by this Interface
        - Interface Class: [1 byte / Class] Class Code (Assigned by USB Org) [Specifies if HID, CDC, MSC]
        - Interface Sub-Class: [1 byte / Sub-Class] Sub-Class Code (Assigned by USB Org) [Specifies if HID, CDC, MSC]
        - Interface Protocol: [1 byte / Protocol] Protocol Code (Assigned by USB Org) [Specifies if HID, CDC, MSC]
        - Interface Index: [1 byte / Index] Index of String Descriptor Describing this interface

    Endpoint Descriptor Composition:
        - Length: [1 byte / number] Size of Descriptor in bytes (7 bytes)
        - Description Type: [1 byte / constant] Interface Descriptor (0x05)
        - Endpoint Address: [1 byte / Bitfield] MSB A000BBBB LSB, If A=1, then endpoint is IN, else OUT (ignored for CONTROL); BBBB=Endpoint Number
        - Attributes: [1 byte / Bitfield] MSB 00AABBTT LSB, TT=Transfer Type, If TT is Isochronous then BB=Synchronization Type and AA=Usage Type
            - TT: 00=Control, 01=Isochronous, 10=Bulk, 11=Interrupt
            - BB: 00=No Synchronization, 01=Asynchronous, 10=Adaptive, 11=Synchronous
            - AA: 00=Data Endpoint, 01=Feedback Endpoint, 10=Explicit Feedback Data Endpoint, 11=Reserved
        - Max Packet Size: [2 bytes / number] Maximum Packet Size this endpoint is capable of sending or receiving
        - Interval: [1 byte / number] Interval for polling endpoint data transfers & Value is in frame counts (1 ms for Low/Full, 125 us for High)
            - Ignored for Bulk & Control Endpoints & Isochronous must equal 1 and field may range from 1 to 255 for interrupt endpoints

    String Descriptor 0 Composition:
        - Length: [1 byte / number] Size of Descriptor in bytes
        - Description Type: [1 byte / constant] String Descriptor (0x03)
        - LANGID[0]: [2 bytes / number] Supported Language Code Zero (Example: 0x0409 English - United States)
        - LANGID[1]: [2 bytes / number] Supported Language Code One (Example: 0x0C09 English - Australian)
        - LANGID[n]: [2 bytes / number] Supported Language Code x (Example: 0x0407 German - Standard)

    General String Descriptor Composition:
        - Length: [1 byte / number] Size of Descriptor in bytes
        - Description Type: [1 byte / constant] String Descriptor (0x03)
        - Data: [n / Unicode] Unicode Encoded String
*/

/* USB Requests & Protocol:
    - Setup Packet [8 bytes]:
        - Request Type [1 byte / bitfield] MSB ABBCCCCC LSB
            - A: Data Phase Transfer Direction, 0=Host to Device, 1=Device to Host
            - BB: Type, 0=Standard, 1=Class, 2=Vendor, 3=Reserved
            - CCCCC: Recipient, 0=Device, 1=Interface, 2=Endpoint, 3=Other, 4-31=Reserved
        - Request [1 byte / Value] Request
        - Value [2 bytes / Value] Value
        - Index [2 bytes / Index or Offset] Index
        - Length [2 bytes / Count] Number of bytes to transfer if there is a data phase

    - Standard Device Requests:
        - Request Type: Device to Host/Standard/Device, Request: GET_STATUS (0x00), Value: Zero, Index: Zero, Length: Two, Data: Device Status
        - Request Type: Host to Device/Standard/Device, Request: CLEAR_FEATURE (0x01), Value: Feature Selector, Index: Zero, Length: Zero, Data: None
        - Request Type: Host to Device/Standard/Device, Request: SET_FEATURE (0x03), Value: Feature Selector, Index: Zero, Length: Zero, Data: None
        - Request Type: Host to Device/Standard/Device, Request: SET_ADDRESS (0x05), Value: Device Address, Index: Zero, Length: Zero, Data: None
        - Request Type: Device to Host/Standard/Device, Request: GET_DESCRIPTOR (0x06), Value: Descriptor Type & Index, Index: Zero or Language ID, Length: Descriptor Length, Data: Descriptor
        - Request Type: Host to Device/Standard/Device, Request: SET_DESCRIPTOR (0x07), Value: Descriptor Type & Index, Index: Zero or Language ID, Length: Descriptor Length, Data: Descriptor
        - Request Type: Device to Host/Standard/Device, Request: GET_CONFIGURATION (0x08), Value: Zero, Index: Zero, Length: One, Data: Configuration Value
        - Request Type: Host to Device/Standard/Device, Request: SET_CONFIGURATION (0x09), Value: Configuration Value, Index: Zero, Length: Zero, Data: None
        - Note: GET_STATUS will return two bytes during the data stage with the following format MSB 00000000000000AB LSB A=Remote-Wakeup, B=Self-Powered
        - Note: SET_FEATURE & CLEAR_FEATURE, when the designated recipient is the device, the only two feature selectors available are DEVICE_REMOTE_WAKEUP and TEST_MODE
        - Note: SET_ADDRESS, used during enumeration to assign a unique address to the USB device
        - Note: SET_DESCRIPTOR & GET_DESCRIPTOR, used to return the specified descriptor. A request for the configuration descriptor will return the device descriptor and all interface and endpoint descriptors in the one request.
            - Endpoint Descriptors cannot be accessed directly by a GetDescriptor/SetDescriptor Request
            - Interface Descriptors cannot be accessed directly by a GetDescriptor/SetDescriptor Request
            - String Descriptors include a Language ID in wIndex to allow for multiple language support
        - Note: GET_CONFIGURATION & SET_CONFIGURATION, used to request or set the current device configuration.
            - In the case of a Get Configuration request, a byte will be returned during the data stage indicating the devices status.
            - A zero value means the device is not configured and a non-zero value indicates the device is configured.
            - Set Configuration is used to enable a device. It should contain the value of ConfigurationValue of the desired configuration descriptor in the lower byte of wValue to select which configuration to enable.

    - Standard Interface Requests:
        - Request Type: Device to Host/Standard/Interface, Request: GET_STATUS (0x00), Value: Zero, Index: Interface, Length: Two, Data: Interface Status
        - Request Type: Host to Device/Standard/Interface, Request: CLEAR_FEATURE (0x01), Value: Feature Selector, Index: Interface, Length: Zero, Data: None
        - Request Type: Host to Device/Standard/Interface, Request: SET_FEATURE (0x03), Value: Feature Selector, Index: Interface, Length: Zero, Data: None
        - Request Type: Device to Host/Standard/Interface, Request: GET_INTERFACE (0x0A), Value: Zero, Index: Interface, Length: One, Data: Alternate Interface
        - Request Type: Host to Device/Standard/Interface, Request: SET_INTERFACE (0x11), Value: Alternative Setting, Index: Interface, Length: Zero, Data: None
        - Note: Index format, MSB RRRRRRRRIIIIIIII LSB, R=Reserved, I=Interface Number
        - Note: Get Status is used to return the status of the interface. Such a request to the interface should return two bytes of 0x00, 0x00. (Both bytes are reserved for future use)
        - Note: SET_FEATURE & CLEAR_FEATURE, requests can be used to set boolean features. When the designated recipient is the interface, the current USB Specification Revision 2 specifies no interface features.
        - Note: GET_INTERFACE & SET_INTERFACE, set the Alternative Interface setting which is described in more detail under the Interface Descriptor.

    - Standard Endpoint Requests:
        - Request Type: Device to Host/Standard/Endpoint, Request: GET_STATUS (0x00), Value: Zero, Index: Endpoint, Length: Two, Data: Endpoint Status
        - Request Type: Host to Device/Standard/Endpoint, Request: CLEAR_FEATURE (0x01), Value: Feature Selector, Index: Endpoint, Length: Zero, Data: None
        - Request Type: Host to Device/Standard/Endpoint, Request: SET_FEATURE (0x03), Value: Feature Selector, Index: Endpoint, Length: Zero, Data: None
        - Request Type: Device to Host/Standard/Endpoint, Request: SYNCH_FRAME (0x12), Value: Zero, Index: Endpoint, Length: Two, Data: Frame Number
        - Note: Index format, MSB RRRRRRRRDRRRNNNN LSB, R=Reserved, D=Direction, N=Endpoint Number
        - Note: Get Status returns two bytes indicating the status (Halted/Stalled) of a endpoint. The format of the two bytes returned is MSB RRRRRRRRRRRRRRRH LSB, R=Reserved, H=Halt
        - Note: CLEAR_FEATURE & SET_FEATURE, used to set Endpoint Features.
            - The standard currently defines one endpoint feature selector, ENDPOINT_HALT (0x00) which allows the host to stall and clear an endpoint.
            - Only endpoints other than the default endpoint are recommended to have this functionality.
        - Note: SYNCH_FRAME, used to report an endpoint synchronization frame
*/

typedef struct
{
    uint8_t length;          // 0x12
    uint8_t descriptor_type; // 0x01
    uint16_t specification;
    uint8_t device_class;
    uint8_t device_sub_class;
    uint8_t device_protocol;
    uint8_t max_packet_size;
    uint16_t vendor_id;
    uint16_t product_id;
    uint16_t device_release_number;
    uint8_t manufacturer_index;
    uint8_t product_index;
    uint8_t serial_number_index;
    uint8_t number_of_configurations;
} usb_device_descriptor;

typedef struct
{
    uint8_t length;          // 0x09
    uint8_t descriptor_type; // 0x02
    uint16_t total_length;
    uint8_t number_of_interfaces;
    uint8_t configuration_value;
    uint8_t configuration_index;
    uint8_t attribute;
    uint8_t max_power;
} usb_configuration_descriptor;

typedef struct
{
    uint8_t length;           // 0x09
    uint8_t descriptor_type;  // 0x04
    uint8_t interface_number;
    uint8_t alternate_setting;
    uint8_t number_of_endpoints;
    uint8_t interface_class;
    uint8_t interface_sub_class;
    uint8_t interface_protocol;
    uint8_t interface_index;
} usb_interface_descriptor;

typedef struct
{
    uint8_t length;           // 0x07
    uint8_t descriptor_type;  // 0x05
    uint8_t endpoint_address;
    uint8_t attributes;
    uint16_t max_packet_size;
    uint8_t interval;
} usb_endpoint_descriptor;

typedef struct
{
    uint8_t length;
    uint8_t descriptor_type;  // 0x03
    uint16_t language_id[0];
} usb_language_descriptor;

typedef struct
{
    uint8_t length;
    uint8_t descriptor_type;  // 0x03
    wchar_t data[0];
} usb_string_descriptor;

typedef struct
{
    uint8_t request_type;
    uint8_t request;
    uint16_t value;
    uint16_t index;
    uint16_t length; // only used if data phase exists
} usb_setup_packet;

// Device Interrupt Flag (INTFLAG)
// Endpoint Interrupt Flag (EPINTFLAG)
// Pipe Interrupt Flag (PINTFLAG)

#endif /* _DRIVERS_USB_H_ */
