#include "drivers/sysctrl.h"

void sysctrl_read_serial_number(uint64_t* out_lower_half, uint64_t* out_upper_half)
{
    uint64_t serial_number_word0 = *(uint32_t*)0x0080A00C;
    uint64_t serial_number_word1 = *(uint32_t*)0x0080A040;
    uint64_t serial_number_word2 = *(uint32_t*)0x0080A044;
    uint64_t serial_number_word3 = *(uint32_t*)0x0080A048;

    *out_lower_half = (serial_number_word1 << 32) | serial_number_word0;
    *out_upper_half = (serial_number_word3 << 32) | serial_number_word2;
}

bool sysctrl_can_enter_standby()
{
    // do not enter standby when an oscillator is in startup!
    if(SYSCTRL->OSC8M.bit.ENABLE && !SYSCTRL->PCLKSR.bit.OSC8MRDY)
    {
        return false;
    }
    if(SYSCTRL->OSC32K.bit.ENABLE && !SYSCTRL->PCLKSR.bit.OSC32KRDY)
    {
        return false;
    }
    if(SYSCTRL->XOSC.bit.ENABLE && !SYSCTRL->PCLKSR.bit.XOSCRDY)
    {
        return false;
    }
    if(SYSCTRL->XOSC32K.bit.ENABLE && !SYSCTRL->PCLKSR.bit.XOSC32KRDY)
    {
        return false;
    }
    if(SYSCTRL->DFLLCTRL.bit.ENABLE && !SYSCTRL->PCLKSR.bit.DFLLRDY)
    {
        return false;
    }
    if(SYSCTRL->DPLLCTRLA.bit.ENABLE && !SYSCTRL->DPLLSTATUS.bit.CLKRDY)
    {
        return false;
    }

    return true;
}

void sysctrl_gosc_set_enabled(sysctrl_clock clk, bool enabled)
{
    switch(clk)
    {
        case CLOCK_XOSC:
            SYSCTRL->XOSC.bit.ENABLE = enabled;
            if(enabled) { while(!SYSCTRL->PCLKSR.bit.XOSCRDY); }
            break;
        case CLOCK_XOSC32K:
            SYSCTRL->XOSC32K.bit.ENABLE = enabled;
            if(enabled) { while(!SYSCTRL->PCLKSR.bit.XOSC32KRDY); }
            break;
        case CLOCK_OSC32K:
            SYSCTRL->OSC32K.bit.ENABLE = enabled;
            if(enabled) { while(!SYSCTRL->PCLKSR.bit.OSC32KRDY); }
            break;
        case CLOCK_OSC8M:
            SYSCTRL->OSC8M.bit.ENABLE = enabled;
            if(enabled) { while(!SYSCTRL->PCLKSR.bit.OSC8MRDY); }
            break;
        case CLOCK_DFLL48M: // this has special start up, it seems that ENABLE *MUST* be set without any other bits set in the same assignment
            if(enabled)
            {
                SYSCTRL->DFLLCTRL.reg = 2;
                while(!SYSCTRL->PCLKSR.bit.DFLLRDY);
            }
            else
            {
                SYSCTRL->DFLLCTRL.reg = 0;
            }
            break;
        case CLOCK_FDPLL96M:
            SYSCTRL->DPLLCTRLA.bit.ENABLE = enabled;
            if(enabled) { while(!SYSCTRL->DPLLSTATUS.bit.CLKRDY); }
            break;
        case CLOCK_OSCULP32K: // always on
        default: return;
    }
}

void sysctrl_gosc_set_on_demand_enabled(sysctrl_clock clk, bool enabled)
{
    switch(clk)
    {
        case CLOCK_XOSC: SYSCTRL->XOSC.bit.ONDEMAND = enabled;
            break;
        case CLOCK_XOSC32K: SYSCTRL->XOSC32K.bit.ONDEMAND = enabled;
            break;
        case CLOCK_OSC32K: SYSCTRL->OSC32K.bit.ONDEMAND = enabled;
            break;
        case CLOCK_OSC8M: SYSCTRL->OSC8M.bit.ONDEMAND = enabled;
            break;
        case CLOCK_DFLL48M: SYSCTRL->DFLLCTRL.bit.ONDEMAND = enabled;
            break;
        case CLOCK_FDPLL96M: SYSCTRL->DPLLCTRLA.bit.ONDEMAND = enabled;
            break;
        case CLOCK_OSCULP32K: // always on
        default: return;
    }
}

void sysctrl_gosc_set_run_in_standby_enabled(sysctrl_clock clk, bool enabled) // oscillator continues to run in standby power mode
{
    switch(clk)
    {
        case CLOCK_XOSC: SYSCTRL->XOSC.bit.RUNSTDBY = enabled;
        break;
        case CLOCK_XOSC32K: SYSCTRL->XOSC32K.bit.RUNSTDBY = enabled;
        break;
        case CLOCK_OSC32K: SYSCTRL->OSC32K.bit.RUNSTDBY = enabled;
        break;
        case CLOCK_OSC8M: SYSCTRL->OSC8M.bit.RUNSTDBY = enabled;
        break;
        case CLOCK_DFLL48M: SYSCTRL->DFLLCTRL.bit.RUNSTDBY = enabled;
        break;
        case CLOCK_FDPLL96M: SYSCTRL->DPLLCTRLA.bit.RUNSTDBY = enabled;
        break;
        case CLOCK_OSCULP32K: // always on
        default: return;
    }
}

void sysctrl_gosc32k_set_32k_mode_enabled(sysctrl_32k_clock clk, bool enabled)
{
    if(clk == CLOCK_32K_OSC32K)
    {
        SYSCTRL->OSC32K.bit.EN32K = enabled;
    }
    else if(clk == CLOCK_32K_XOSC32K)
    {
        SYSCTRL->XOSC32K.bit.EN32K = enabled;
    }
}

void sysctrl_gosc32k_set_1k_mode_enabled(sysctrl_32k_clock clk, bool enabled)
{
    if(clk == CLOCK_32K_OSC32K)
    {
        SYSCTRL->OSC32K.bit.EN1K = enabled;
    }
    else if(clk == CLOCK_32K_XOSC32K)
    {
        SYSCTRL->XOSC32K.bit.EN1K = enabled;
    }
}

void sysctrl_gxosc_set_ext_mode(sysctrl_ext_clock clk, sysctrl_ext_mode mode)
{
    if(clk == EXT_CLOCK_XOSC)
    {
        SYSCTRL->XOSC.bit.XTALEN = (mode == MODE_EXT_CRYSTAL);
    }
    else if(clk == EXT_CLOCK_XOSC32K)
    {
        SYSCTRL->XOSC32K.bit.XTALEN = (mode == MODE_EXT_CRYSTAL);
    }
}

void sysctrl_gxosc_set_auto_amp_gain_control_enabled(sysctrl_ext_clock clk, bool enabled)
{
    if(clk == EXT_CLOCK_XOSC)
    {
        while(!SYSCTRL->PCLKSR.bit.XOSCRDY);
        SYSCTRL->XOSC.bit.AMPGC = enabled;
    }
    else if(clk == EXT_CLOCK_XOSC32K)
    {
        SYSCTRL->XOSC32K.bit.AAMPEN = enabled;
    }
}

void sysctrl_xosc_set_start_up_time(sysctrl_xosc_startup_time startup_time)
{
    SYSCTRL->XOSC.bit.STARTUP = startup_time;
}

void sysctrl_xosc32k_set_start_up_time(sysctrl_xosc32k_startup_time startup_time)
{
    SYSCTRL->XOSC32K.bit.STARTUP = startup_time;
}

void sysctrl_xosc_set_gain(sysctrl_xosc_gain gain)
{
    SYSCTRL->XOSC.bit.GAIN = gain;
}

void sysctrl_xosc32k_enable_write_lock()
{
    SYSCTRL->XOSC32K.bit.WRTLOCK = true;
}

void sysctrl_osc32k_enable_write_lock()
{
    SYSCTRL->OSC32K.bit.WRTLOCK = true;
}

void sysctrl_osculp32k_enable_write_lock()
{
    SYSCTRL->OSCULP32K.bit.WRTLOCK = true;
}

void sysctrl_osc32k_set_start_up_time(sysctrl_osc32k_startup_time startup_time)
{
    SYSCTRL->OSC32K.bit.STARTUP = startup_time;
}

void sysctrl_osc8m_set_prescalar(sysctrl_osc8m_prescalar prescalar)
{
    SYSCTRL->OSC8M.bit.PRESC = prescalar;
}

void sysctrl_osc32k_load_factory_calibration()
{
    uint64_t nvm_software_calibration_area = *(uint64_t*)0x00806020;
    uint8_t osc32k_cal = ((nvm_software_calibration_area & 0x00001FC000000000) >> 38);
    SYSCTRL->OSC32K.bit.CALIB = osc32k_cal;
    if(SYSCTRL->OSC32K.bit.ENABLE)
    {
        while(!SYSCTRL->PCLKSR.bit.OSC32KRDY);
    }
}

bool sysctrl_osc8m_get_ready_flag()
{
    return SYSCTRL->PCLKSR.bit.OSC8MRDY;
}

bool sysctrl_osc32k_get_ready_flag()
{
    return SYSCTRL->PCLKSR.bit.OSC32KRDY;
}

bool sysctrl_xosc_get_ready_flag()
{
    return SYSCTRL->PCLKSR.bit.XOSCRDY;
}

bool sysctrl_xosc32k_get_ready_flag()
{
    return SYSCTRL->PCLKSR.bit.XOSC32KRDY;
}

void sysctrl_bod33_set_run_in_standby_enabled(bool enabled)
{
    SYSCTRL->BOD33.bit.RUNSTDBY = enabled;
    while(!SYSCTRL->PCLKSR.bit.B33SRDY);
}

void sysctrl_bod33_set_operation_mode(sysctrl_bod33_mode mode)
{
    SYSCTRL->BOD33.bit.MODE = mode;
    while(!SYSCTRL->PCLKSR.bit.B33SRDY);
}

void sysctrl_bod33_set_sampling_clock_enabled(bool enabled)
{
    SYSCTRL->BOD33.bit.CEN = enabled;
    while(!SYSCTRL->PCLKSR.bit.B33SRDY);
}

void sysctrl_bod33_set_prescalar(sysctrl_bod33_prescalar prescalar)
{
    SYSCTRL->BOD33.bit.PSEL = prescalar;
    while(!SYSCTRL->PCLKSR.bit.B33SRDY);
}

void sysctrl_bod33_set_action(sysctrl_bod33_action action)
{
    SYSCTRL->BOD33.bit.ACTION = action;
    while(!SYSCTRL->PCLKSR.bit.B33SRDY);
}

void sysctrl_bod33_set_level(sysctrl_bod33_level level)
{
    bool state = SYSCTRL->BOD33.bit.ENABLE;
    SYSCTRL->BOD33.bit.LEVEL = level;
    SYSCTRL->BOD33.bit.ENABLE = state;
    while(!SYSCTRL->PCLKSR.bit.B33SRDY);
}

void sysctrl_bod33_set_hysteresis_enabled(bool enabled)
{
    SYSCTRL->BOD33.bit.HYST = enabled;
    while(!SYSCTRL->PCLKSR.bit.B33SRDY);
}

void sysctrl_bod33_set_enabled(bool enabled)
{
    SYSCTRL->BOD33.bit.ENABLE = enabled;
    while(!SYSCTRL->PCLKSR.bit.B33SRDY);
}

bool sysctrl_bod33_get_synchronizing_flag()
{
    return SYSCTRL->PCLKSR.bit.B33SRDY;
}

bool sysctrl_bod33_get_detection_flag()
{
    return SYSCTRL->PCLKSR.bit.BOD33DET;
}

bool sysctrl_bod33_get_ready_flag()
{
    return SYSCTRL->PCLKSR.bit.BOD33RDY;
}

void sysctrl_vreg_set_high_drive_enabled(bool enabled)
{
    SYSCTRL->VREG.bit.FORCELDO = enabled;
}

void sysctrl_vreg_set_run_in_standby_enabled(bool enabled)
{
    SYSCTRL->VREG.bit.RUNSTDBY = enabled;
}

void sysctrl_vref_set_bandgap_output_enabled(bool enabled)
{
    SYSCTRL->VREF.bit.BGOUTEN = enabled;
}

void sysctrl_vref_set_temperature_sensor_enabled(bool enabled)
{
    SYSCTRL->VREF.bit.TSEN = enabled;
}

void sysctrl_dfll48m_set_wait_for_lock_enabled(bool enabled)
{
    SYSCTRL->DFLLCTRL.bit.WAITLOCK = enabled;
    while(!SYSCTRL->PCLKSR.bit.DFLLRDY);
}

void sysctrl_dfll48m_set_coarse_lock_bypass_enabled(bool enabled)
{
    SYSCTRL->DFLLCTRL.bit.BPLCKC = enabled;
    while(!SYSCTRL->PCLKSR.bit.DFLLRDY);
}

void sysctrl_dfll48m_set_quick_lock_enabled(bool enabled)
{
    SYSCTRL->DFLLCTRL.bit.QLDIS = !enabled;
    while(!SYSCTRL->PCLKSR.bit.DFLLRDY);
}

void sysctrl_dfll48m_set_chill_cycle_enabled(bool enabled)
{
    SYSCTRL->DFLLCTRL.bit.CCDIS = !enabled;
    while(!SYSCTRL->PCLKSR.bit.DFLLRDY);
}

void sysctrl_dfll48m_set_usb_clock_recovery_mode_enabled(bool enabled)
{
    SYSCTRL->DFLLCTRL.bit.USBCRM = enabled;
    while(!SYSCTRL->PCLKSR.bit.DFLLRDY);
}

void sysctrl_dfll48m_set_lose_lock_after_wake_enabled(bool enabled)
{
    SYSCTRL->DFLLCTRL.bit.LLAW = enabled;
    while(!SYSCTRL->PCLKSR.bit.DFLLRDY);
}

void sysctrl_dfll48m_set_stable_dfll_frequency_enabled(bool enabled)
{
    SYSCTRL->DFLLCTRL.bit.STABLE = enabled;
    while(!SYSCTRL->PCLKSR.bit.DFLLRDY);
}

void sysctrl_dfll48m_set_closed_loop_mode_enabled(bool enabled)
{
    SYSCTRL->DFLLCTRL.bit.MODE = enabled;
    while(!SYSCTRL->PCLKSR.bit.DFLLRDY);
}

uint16_t sysctrl_dfll48m_get_closed_loop_multiplication_ratio_difference()
{
    SYSCTRL->DFLLSYNC.bit.READREQ = 1;
    while(!SYSCTRL->PCLKSR.bit.DFLLRDY);
    return SYSCTRL->DFLLVAL.bit.DIFF;
}

void sysctrl_dfll48m_load_open_loop_coarse_factory_calibration()
{
    uint64_t nvm_software_calibration_area = *(uint64_t*)0x00806020;
    uint8_t dfll48m_coarse_cal = ((nvm_software_calibration_area & 0xFC00000000000000) >> 58);
    SYSCTRL->DFLLVAL.bit.COARSE = dfll48m_coarse_cal;
    while(!SYSCTRL->PCLKSR.bit.DFLLRDY);
}

void sysctrl_dfll48m_set_open_loop_coarse_value(uint8_t coarse)
{
    // not my job to prevent overflow / stupid users
    // if(coarse > 0x3F) { return; }

    SYSCTRL->DFLLVAL.bit.COARSE = coarse;
    while(!SYSCTRL->PCLKSR.bit.DFLLRDY);
}

void sysctrl_dfll48m_set_open_loop_fine_value(uint16_t fine)
{
    // not my job to prevent overflow / stupid users
    //if(fine > 0x03FF) { return; }

    SYSCTRL->DFLLVAL.bit.FINE = fine;
    while(!SYSCTRL->PCLKSR.bit.DFLLRDY);
}

void sysctrl_dfll48m_set_coarse_max_step(uint8_t cstep)
{
    SYSCTRL->DFLLMUL.bit.CSTEP = cstep;
    while(!SYSCTRL->PCLKSR.bit.DFLLRDY);
}

void sysctrl_dfll48m_set_fine_max_step(uint16_t fstep)
{
    SYSCTRL->DFLLMUL.bit.FSTEP = fstep;
    while(!SYSCTRL->PCLKSR.bit.DFLLRDY);
}

void sysctrl_dfll48m_set_dfll_multiply_factor(uint16_t mul)
{
    SYSCTRL->DFLLMUL.bit.MUL = mul;
    while(!SYSCTRL->PCLKSR.bit.DFLLRDY);
}

uint32_t sysctrl_dfll48m_get_closed_loop_mode_value()
{
    SYSCTRL->DFLLSYNC.bit.READREQ = 1;
    while(!SYSCTRL->PCLKSR.bit.DFLLRDY);
    return SYSCTRL->DFLLVAL.reg;
}

bool sysctrl_dfll48m_get_reference_clock_stopped_flag()
{
    return SYSCTRL->PCLKSR.bit.DFLLRCS;
}

bool sysctrl_dfll48m_get_coarse_lock_detected_flag()
{
    return SYSCTRL->PCLKSR.bit.DFLLLCKC;
}

bool sysctrl_dfll48m_get_fine_lock_detected_flag()
{
    return SYSCTRL->PCLKSR.bit.DFLLLCKF;
}

bool sysctrl_dfll48m_get_out_of_bounds_detected_flag()
{
    return SYSCTRL->PCLKSR.bit.DFLLOOB;
}

bool sysctrl_dfll48m_get_ready_flag()
{
    return SYSCTRL->PCLKSR.bit.DFLLRDY;
}

void sysctrl_fdpll96m_set_loop_divider_ratio(uint16_t ldr)
{
    SYSCTRL->DPLLRATIO.bit.LDR = ldr;
}

void sysctrl_fdpll96m_set_loop_divider_ratio_fractional_part(uint8_t ldrfrac)
{
    SYSCTRL->DPLLRATIO.bit.LDRFRAC = ldrfrac;
}

void sysctrl_fdpll96m_set_lock_bypass_enabled(bool enabled)
{
    SYSCTRL->DPLLCTRLB.bit.LBYPASS = enabled;
}

void sysctrl_fdpll96m_set_lock_time(sysctrl_fdpll96m_lock_time lock_time)
{
    SYSCTRL->DPLLCTRLB.bit.LTIME = lock_time;
}

void sysctrl_fdpll96m_set_reference_clock(sysctrl_fdpll96m_reference_clock clk)
{
    SYSCTRL->DPLLCTRLB.bit.REFCLK = clk;
}

void sysctrl_fdpll96m_set_wake_up_fast_enabled(bool enabled)
{
    SYSCTRL->DPLLCTRLB.bit.WUF = enabled;
}

void sysctrl_fdpll96m_set_low_power_enabled(bool enabled)
{
    SYSCTRL->DPLLCTRLB.bit.LPEN = enabled;
}

void sysctrl_fdpll96m_set_filter_mode(sysctrl_fdpll96m_filter_mode mode)
{
    SYSCTRL->DPLLCTRLB.bit.FILTER = mode;
}

bool sysctrl_fdpll96m_get_reference_clock_divider_enabled_flag()
{
    return SYSCTRL->DPLLSTATUS.bit.DIV;
}

bool sysctrl_fdpll96m_get_dpll_enabled_flag()
{
    return SYSCTRL->DPLLSTATUS.bit.ENABLE;
}

bool sysctrl_fdpll96m_get_output_clock_ready_flag()
{
    return SYSCTRL->DPLLSTATUS.bit.CLKRDY;
}

bool sysctrl_fdpll96m_get_lock_signal_flag()
{
    return SYSCTRL->DPLLSTATUS.bit.LOCK;
}

bool sysctrl_fdpll96m_get_lock_timeout_detected_flag()
{
    return SYSCTRL->PCLKSR.bit.DPLLLTO;
}

bool sysctrl_fdpll96m_get_lock_fall_edge_detected_flag()
{
    return SYSCTRL->PCLKSR.bit.DPLLLCKF;
}

bool sysctrl_fdpll96m_get_lock_rise_edge_detected_flag()
{
    return SYSCTRL->PCLKSR.bit.DPLLLCKR;
}
