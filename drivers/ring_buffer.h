
#ifndef _GENERAL_RING_BUFFER_H_
#define _GENERAL_RING_BUFFER_H_

#include <stdint.h>
#include <stdbool.h>

typedef struct
{
    uint8_t* buffer;
    uint8_t* head;
    uint8_t* tail;
    uint32_t size;
    uint32_t element_size;
    uint32_t count;
} generic_ring_buffer;

typedef enum
{
    GRBE_NONE,
    GRBE_NOT_WRITTEN,
    GRBE_OVER_WRITTEN
} generic_ring_buffer_error;

extern const char* generic_ring_buffer_error_str[3];

void                      generic_ring_buffer_init         (generic_ring_buffer* rbuf, void* buffer, uint32_t element_size, uint32_t buffer_size);
void                      generic_ring_buffer_reset        (generic_ring_buffer* rbuf);
generic_ring_buffer_error generic_ring_buffer_put          (generic_ring_buffer* rbuf, void* data, bool overwrite);
generic_ring_buffer_error generic_ring_buffer_get          (generic_ring_buffer* rbuf, void* data);
generic_ring_buffer_error generic_ring_buffer_advance_tail (generic_ring_buffer* rbuf);
generic_ring_buffer_error generic_ring_buffer_peek         (generic_ring_buffer* rbuf, void* data);
bool                      generic_ring_buffer_empty        (generic_ring_buffer* rbuf);
bool                      generic_ring_buffer_full         (generic_ring_buffer* rbuf);
uint32_t                  generic_ring_get_count           (generic_ring_buffer* rbuf);

#endif /* INC_GENERAL_RING_BUFFER_H_ */
