#ifndef _DRIVERS_GCLK_H_
#define _DRIVERS_GCLK_H_

#include "drivers/defs.h"

typedef enum
{
    GCLK_GENERATOR_0 = 0x0, // always the source for GCLK_MAIN, goes to PM
    GCLK_GENERATOR_1 = 0x1, // only GCLK that can act as input to all other GCLKs except itself
    GCLK_GENERATOR_2 = 0x2,
    GCLK_GENERATOR_3 = 0x3,
    GCLK_GENERATOR_4 = 0x4,
    GCLK_GENERATOR_5 = 0x5,
    GCLK_GENERATOR_6 = 0x6,
    GCLK_GENERATOR_7 = 0x7,
    GCLK_GENERATOR_8 = 0x8
} gclk_generator;

typedef enum
{
    GCLK_SRC_XOSC        = 0x0,
    GCLK_SRC_IO_PAD      = 0x1,
    GCLK_SRC_GCLK_1      = 0x2,
    GCLK_SRC_OSCULP32K   = 0x3,
    GCLK_SRC_OSC32K      = 0x4,
    GCLK_SRC_XOSC32K     = 0x5,
    GCLK_SRC_OSC8M       = 0x6,
    GCLK_SRC_DFLL48M     = 0x7,
    GCLK_SRC_FDPLL96M    = 0x8
} gclk_source;

typedef enum
{
    GCLK_ID_DFLL48         = 0x00, // Max: 33 kHz
    GCLK_ID_FDPLL          = 0x01, // Max:  2 MHz
    GCLK_ID_FDPLL32K       = 0x02, // Max: 32 kHz
    GCLK_ID_WDT            = 0x03, // Max: 48 MHz
    GCLK_ID_RTC            = 0x04, // Max: 48 MHz
    GCLK_ID_EIC            = 0x05, // Max: 48 MHz
    GCLK_ID_USB            = 0x06, // Max: 48 MHz
    GCLK_ID_EVSYS_0        = 0x07, // Max: 48 MHz
    GCLK_ID_EVSYS_1        = 0x08, // Max: 48 MHz
    GCLK_ID_EVSYS_2        = 0x09, // Max: 48 MHz
    GCLK_ID_EVSYS_3        = 0x0A, // Max: 48 MHz
    GCLK_ID_EVSYS_4        = 0x0B, // Max: 48 MHz
    GCLK_ID_EVSYS_5        = 0x0C, // Max: 48 MHz
    GCLK_ID_EVSYS_6        = 0x0D, // Max: 48 MHz
    GCLK_ID_EVSYS_7        = 0x0E, // Max: 48 MHz
    GCLK_ID_EVSYS_8        = 0x0F, // Max: 48 MHz
    GCLK_ID_EVSYS_9        = 0x10, // Max: 48 MHz
    GCLK_ID_EVSYS_10       = 0x11, // Max: 48 MHz
    GCLK_ID_EVSYS_11       = 0x12, // Max: 48 MHz
    GCLK_ID_SERCOMX_SLOW   = 0x13, // Max: 48 MHz
    GCLK_ID_SERCOM0_CORE   = 0x14, // Max: 48 MHz
    GCLK_ID_SERCOM1_CORE   = 0x15, // Max: 48 MHz
    GCLK_ID_SERCOM2_CORE   = 0x16, // Max: 48 MHz
    GCLK_ID_SERCOM3_CORE   = 0x17, // Max: 48 MHz
    GCLK_ID_SERCOM4_CORE   = 0x18, // Max: 48 MHz
    GCLK_ID_SERCOM5_CORE   = 0x19, // Max: 48 MHz
    GCLK_ID_TCC0_TCC1      = 0x1A, // Max: 96 MHz
    GCLK_ID_TCC2_TC3       = 0x1B, // Max: 96 MHz
    GCLK_ID_TC4_TC5        = 0x1C, // Max: 48 MHz
    GCLK_ID_TC6_TC7        = 0x1D, // Max: 48 MHz
    GCLK_ID_ADC            = 0x1E, // Max: 48 MHz
    GCLK_ID_AC_DIG_AC1_DIG = 0x1F, // Max: 48 MHz
    GCLK_ID_AC_ANA_AC1_ANA = 0x20, // Max: 64 kHz
    GCLK_ID_DAC            = 0x21, // Max: 350 kHz
    GCLK_ID_PTC            = 0x22, // Max: 48 MHz
    GCLK_ID_I2S_0          = 0x23, // Max: 13 MHz
    GCLK_ID_I2S_1          = 0x24, // Max: 13 MHz
    GCLK_ID_TCC3           = 0x25  // Max: 96 MHz
} gclk_output;

typedef enum
{
    GCLK_DIVM_0, // divided directly by the division value
    GCLK_DIVM_1  // divided by 2^(the division value + 1)
} gclk_division_method;

void gclk_swreset(void);
void gclk_set_gen_enabled(gclk_generator gclk_gen, bool enabled);
void gclk_set_clk_enabled(gclk_output gclk_out, bool enabled);
void gclk_set_gen_source(gclk_generator gclk_gen, gclk_source gclk_src);
void gclk_set_clk_source_gen(gclk_output gclk_out, gclk_generator gclk_gen);
void gclk_set_gen_division_factor(gclk_generator gclk_gen, uint16_t gclk_div);
void gclk_set_gen_division_method(gclk_generator gclk_gen, gclk_division_method gclk_divm);
void gclk_set_gen_run_in_standby_enabled(gclk_generator gclk_gen, bool enabled);
void gclk_set_gen_output_to_io_enabled(gclk_generator gclk_gen, bool enabled); // do not turn this on if the gclk's input is set to GCLK_SRC_IO_PAD
void gclk_set_gen_output_io_resting_level(gclk_generator gclk_gen, bool rest_high);
void gclk_set_gen_improve_duty_cycle_enabled(gclk_generator gclk_gen, bool enabled);
void gclk_enable_clk_write_lock(gclk_output gclk_out); // can only be disabled by a power reset, and GCLK_GENERATOR_0 cannot be locked

#endif /* _DRIVERS_GCLK_H_ */
