#include "drivers/gclk.h"

#define GCLK_SELECT_VIA_8BIT(reg, val) *(uint8_t*)&reg = (uint8_t)val

void gclk_swreset()
{
    while(GCLK->STATUS.bit.SYNCBUSY);
    GCLK->CTRL.bit.SWRST = 1;
    while(GCLK->STATUS.bit.SYNCBUSY || GCLK->CTRL.bit.SWRST);
}

void gclk_set_gen_enabled(gclk_generator gclk_gen, bool enabled)
{
    GCLK_SELECT_VIA_8BIT(GCLK->GENCTRL.reg, gclk_gen);
    while(GCLK->STATUS.bit.SYNCBUSY);
    GCLK->GENCTRL.bit.GENEN = enabled;
    while(GCLK->STATUS.bit.SYNCBUSY);
}

void gclk_set_clk_enabled(gclk_output gclk_out, bool enabled)
{
    GCLK_SELECT_VIA_8BIT(GCLK->CLKCTRL.reg, gclk_out);
    while(GCLK->STATUS.bit.SYNCBUSY);
    GCLK->CLKCTRL.bit.CLKEN = enabled;
    while(GCLK->STATUS.bit.SYNCBUSY);
}

void gclk_set_gen_source(gclk_generator gclk_gen, gclk_source gclk_src)
{
    GCLK_SELECT_VIA_8BIT(GCLK->GENCTRL.reg, gclk_gen);
    while(GCLK->STATUS.bit.SYNCBUSY);
    GCLK->GENCTRL.bit.SRC = gclk_src;
    while(GCLK->STATUS.bit.SYNCBUSY);
}

void gclk_set_clk_source_gen(gclk_output gclk_out, gclk_generator gclk_gen)
{
    GCLK_SELECT_VIA_8BIT(GCLK->CLKCTRL.reg, gclk_out);
    while(GCLK->STATUS.bit.SYNCBUSY);
    GCLK->CLKCTRL.bit.GEN = gclk_gen;
    while(GCLK->STATUS.bit.SYNCBUSY);
}

void gclk_set_gen_division_factor(gclk_generator gclk_gen, uint16_t gclk_div)
{
    GCLK_SELECT_VIA_8BIT(GCLK->GENDIV.reg, gclk_gen);
    while(GCLK->STATUS.bit.SYNCBUSY);
    GCLK->GENDIV.bit.DIV = gclk_div;
    while(GCLK->STATUS.bit.SYNCBUSY);
}

void gclk_set_gen_division_method(gclk_generator gclk_gen, gclk_division_method gclk_divm)
{
    GCLK_SELECT_VIA_8BIT(GCLK->GENCTRL.reg, gclk_gen);
    while(GCLK->STATUS.bit.SYNCBUSY);
    GCLK->GENCTRL.bit.DIVSEL = gclk_divm == GCLK_DIVM_1;
    while(GCLK->STATUS.bit.SYNCBUSY);
}

void gclk_set_gen_run_in_standby_enabled(gclk_generator gclk_gen, bool enabled)
{
    GCLK_SELECT_VIA_8BIT(GCLK->GENCTRL.reg, gclk_gen);
    while(GCLK->STATUS.bit.SYNCBUSY);
    GCLK->GENCTRL.bit.RUNSTDBY = enabled;
    while(GCLK->STATUS.bit.SYNCBUSY);
}

void gclk_set_gen_output_to_io_enabled(gclk_generator gclk_gen, bool enabled)
{
    GCLK_SELECT_VIA_8BIT(GCLK->GENCTRL.reg, gclk_gen);
    while(GCLK->STATUS.bit.SYNCBUSY);
    GCLK->GENCTRL.bit.OE = enabled;
    while(GCLK->STATUS.bit.SYNCBUSY);
}

void gclk_set_gen_output_io_resting_level(gclk_generator gclk_gen, bool rest_high)
{
    GCLK_SELECT_VIA_8BIT(GCLK->GENCTRL.reg, gclk_gen);
    while(GCLK->STATUS.bit.SYNCBUSY);
    GCLK->GENCTRL.bit.OOV = rest_high;
    while(GCLK->STATUS.bit.SYNCBUSY);
}

void gclk_set_gen_improve_duty_cycle_enabled(gclk_generator gclk_gen, bool enabled)
{
    GCLK_SELECT_VIA_8BIT(GCLK->GENCTRL.reg, gclk_gen);
    while(GCLK->STATUS.bit.SYNCBUSY);
    GCLK->GENCTRL.bit.IDC = enabled;
    while(GCLK->STATUS.bit.SYNCBUSY);
}

void gclk_enable_clk_write_lock(gclk_output gclk_out)
{
    GCLK_SELECT_VIA_8BIT(GCLK->CLKCTRL.reg, gclk_out);
    while(GCLK->STATUS.bit.SYNCBUSY);
    GCLK->CLKCTRL.bit.WRTLOCK = 1;
    while(GCLK->STATUS.bit.SYNCBUSY);
}
