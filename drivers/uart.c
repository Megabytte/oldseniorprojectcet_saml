#include "drivers/uart.h"
#include "drivers/delay.h"

volatile struct
{
    uint16_t* tx;
    uint16_t* rx;
    uint32_t tx_length, tx_index;
    uint32_t rx_length, rx_index;
} usart_state[6];

// Driver Callbacks
void (*usart_error_callback)(uint8_t sercom_index, usart_error error) = NULL;
void (*usart_receive_started_callback)(uint8_t sercom_index) = NULL;
void (*usart_cts_changed_callback)(uint8_t sercom_index, bool cts_state) = NULL;
void (*usart_break_received_callback)(uint8_t sercom_index) = NULL;
void (*usart_receive_complete_callback)(uint8_t sercom_index, uint16_t* received_data, uint32_t data_length, bool nine_bit_mode) = NULL;
void (*usart_receive_character_callback)(uint8_t sercom_index, uint16_t received_char, bool nine_bit_mode) = NULL;
void (*usart_transmit_complete_callback)(uint8_t sercom_index, uint16_t* sent_data, uint32_t data_length, bool nine_bit_mode) = NULL;

// Private Driver Functions
uint8_t get_sercom_index(Sercom* usart);
IRQn_Type get_sercom_irq(Sercom* usart);

uint8_t get_sercom_index(Sercom* usart)
{
    if(usart == SERCOM0)
    {
        return 0;
    }

    if(usart == SERCOM1)
    {
        return 1;
    }

    if(usart == SERCOM2)
    {
        return 2;
    }

    if(usart == SERCOM3)
    {
        return 3;
    }

    #ifdef ID_SERCOM4
    if(usart == SERCOM4)
    {
        return 4;
    }
    #endif

    #ifdef ID_SERCOM5
    if(usart == SERCOM5)
    {
        return 5;
    }
    #endif

    // critical driver error : no memory safe action other than to freeze or crash
    for(;;);
}

IRQn_Type get_sercom_irq(Sercom* usart)
{
    if(usart == SERCOM0)
    {
        return SERCOM0_IRQn;
    }

    if(usart == SERCOM1)
    {
        return SERCOM1_IRQn;
    }

    if(usart == SERCOM2)
    {
        return SERCOM2_IRQn;
    }

    if(usart == SERCOM3)
    {
        return SERCOM3_IRQn;
    }

    #ifdef ID_SERCOM4
    if(usart == SERCOM4)
    {
        return SERCOM4_IRQn;
    }
    #endif

    #ifdef ID_SERCOM5
    if(usart == SERCOM5)
    {
        return SERCOM5_IRQn;
    }
    #endif

    // critical driver error : no memory safe action other than to freeze or crash
    for(;;);
}

void usart_set_clock_source(Sercom* usart, usart_clock_source clk_src)
{
    usart->USART.CTRLA.bit.MODE = clk_src;
}

void usart_set_sync_mode(Sercom* usart, usart_sync_mode mode)
{
    usart->USART.CTRLA.bit.CMODE = mode;
}

void usart_set_pinout(Sercom* usart, usart_pinout pinout)
{
    usart->USART.CTRLA.bit.RXPO = (pinout & 0xF0) >> 4;
    usart->USART.CTRLA.bit.TXPO = pinout & 0x0F;
}

void usart_set_character_size(Sercom* usart, usart_character_size size)
{
    usart->USART.CTRLB.bit.CHSIZE = size;
}

void usart_set_data_order(Sercom* usart, usart_data_order order)
{
    usart->USART.CTRLA.bit.DORD = order;
}

void usart_set_parity_mode(Sercom* usart, usart_parity_mode mode)
{
    usart->USART.CTRLB.bit.PMODE = mode;
}

void usart_set_stop_bits(Sercom* usart, usart_stop_bits bits)
{
    usart->USART.CTRLB.bit.SBMODE = bits;
}

void usart_set_baudrate(Sercom* usart, uint32_t sercom_clk_rate, uint32_t desired_baudrate)
{
    if(usart->USART.CTRLA.bit.CMODE == USART_ASYNCHRONOUS)
    {
        if(usart->USART.CTRLA.bit.SAMPR & 1)
        {
            uint32_t S = usart->USART.CTRLA.bit.SAMPR & 2 ? 8 : 16;
            uint32_t mul_ratio = (uint64_t)((uint64_t)sercom_clk_rate*(uint64_t)1000)/(uint64_t)(desired_baudrate*S);
            uint32_t baud = mul_ratio/1000;
            uint32_t fp = ((mul_ratio - (baud*1000))*8)/1000;
            usart->USART.BAUD.FRACFP.BAUD = baud;
            usart->USART.BAUD.FRACFP.FP = fp;
        }
        else
        {
            // Asynchronous Arithmetic : fBAUD=(fREF/16)*(1-(BAUD/65536)) , BAUD=65536*(1-16*(fBAUD/fREF))
            usart->USART.BAUD.reg = (uint16_t)(65536 * (1 - 16 * ((float)desired_baudrate / sercom_clk_rate)));
        }
    }
    else
    {
        if(usart->USART.CTRLA.bit.SAMPR & 1)
        {
            // unknown if this is correct
            uint32_t S = usart->USART.CTRLA.bit.SAMPR & 2 ? 8 : 16;
            uint32_t mul_ratio = (uint64_t)((uint64_t)sercom_clk_rate*(uint64_t)1000)/(uint64_t)(desired_baudrate*S);
            uint32_t baud = mul_ratio/1000;
            uint32_t fp = ((mul_ratio - (baud*1000))*8)/1000;
            usart->USART.BAUD.FRACFP.BAUD = baud;
            usart->USART.BAUD.FRACFP.FP = fp;
        }
        else
        {
            // Synchronous Arithmetic  : fBAUD=fREF/(2*(BAUD+1))          , BAUD=(fREF/(2*fBAUD))-1
            usart->USART.BAUD.reg = (uint16_t)((float)sercom_clk_rate / (2 * desired_baudrate)) - 1;
        }
    }
}

uint32_t usart_get_baudrate(Sercom* usart, uint32_t sercom_clk_rate)
{
    if(usart->USART.CTRLA.bit.CMODE == USART_ASYNCHRONOUS)
    {
        if(usart->USART.CTRLA.bit.SAMPR & 1)
        {
            uint32_t S = usart->USART.CTRLA.bit.SAMPR & 2 ? 8 : 16;
            // fBAUD=fREF/(S*(BAUD+(FP/8))) ; S=# of samples per bit
            return (uint32_t)(sercom_clk_rate / (float)(S * (usart->USART.BAUD.FRACFP.BAUD + ((float)usart->USART.BAUD.FRACFP.FP / 8))));
        }
        else
        {
            // Asynchronous Arithmetic : fBAUD=(fREF/16)*(1-(BAUD/65536)) , BAUD=65536*(1-16*(fBAUD/fREF))
            return (uint32_t)((float)sercom_clk_rate / 16) * (1 - ((float)usart->USART.BAUD.reg / 65536));
        }
    }
    else
    {
        if(usart->USART.CTRLA.bit.SAMPR & 1)
        {
            // unknown if this is correct

            uint32_t S = usart->USART.CTRLA.bit.SAMPR & 2 ? 8 : 16;
            // fBAUD=fREF/(S*(BAUD+(FP/8))) ; S=# of samples per bit
            return (uint32_t)(sercom_clk_rate / (float)(S * (usart->USART.BAUD.FRACFP.BAUD + ((float)usart->USART.BAUD.FRACFP.FP / 8))));
        }
        else
        {
            // Synchronous             : fBAUD=fREF/(2*(BAUD+1))          , BAUD=(fREF/(2*fBAUD))-1
            return (uint32_t)((float)sercom_clk_rate / (2 * (usart->USART.BAUD.reg + 1)));
        }
    }
}

void usart_set_transceiver_enabled(Sercom* usart, bool transmitter_enabled, bool receiver_enabled)
{
    usart->USART.CTRLB.bit.TXEN = transmitter_enabled;
    while(usart->USART.SYNCBUSY.bit.CTRLB);
    usart->USART.CTRLB.bit.RXEN = receiver_enabled;
    while(usart->USART.SYNCBUSY.bit.CTRLB);
}

void usart_set_enabled(Sercom* usart, bool enabled)
{
    usart->USART.CTRLA.bit.ENABLE = enabled;
    while(usart->USART.SYNCBUSY.bit.ENABLE);
}

void usart_swreset(Sercom* usart)
{
    usart->USART.CTRLA.bit.SWRST = 1;
    while(usart->USART.CTRLA.bit.SWRST || usart->USART.SYNCBUSY.bit.SWRST);
}

void usart_set_clock_polarity(Sercom* usart, usart_clock_polarity polarity)
{
    usart->USART.CTRLA.bit.CPOL = polarity;
}

void usart_set_frame_format(Sercom* usart, usart_frame_format format)
{
    usart->USART.CTRLA.bit.FORM = format;
}

void usart_set_oversampling_rate(Sercom* usart, usart_oversampling_rate rate)
{
    usart->USART.CTRLA.bit.SAMPR = rate;
}

void usart_set_oversampling_adjustment_mode(Sercom* usart, usart_oversampling_adjustment_mode mode)
{
    usart->USART.CTRLA.bit.SAMPA = mode;
}

void usart_set_immediate_buffer_overflow_notification_enabled(Sercom* usart, bool enabled)
{
    usart->USART.CTRLA.bit.IBON = enabled;
}

void usart_set_run_in_standby_enabled(Sercom* usart, bool enabled)
{
    usart->USART.CTRLA.bit.RUNSTDBY = enabled;
}

void usart_set_irda_encoding_enabled(Sercom* usart, bool enabled)
{
    usart->USART.CTRLB.bit.ENC = enabled;
}

void usart_set_irda_receive_pulse_length(Sercom* usart, uint8_t pulse)
{
    // Ex: pulse=19 -> 20 SE Clock Cycles. Using 16x Oversampling -> 20/160 = 2/16 minimum pulse width
    usart->USART.RXPL.reg = pulse;
}

void usart_set_collision_detection_enabled(Sercom* usart, bool enabled)
{
    usart->USART.CTRLB.bit.COLDEN = enabled;
}

void usart_set_start_of_frame_detection_enabled(Sercom* usart, bool enabled)
{
    usart->USART.CTRLB.bit.SFDE = enabled;
}

usart_error usart_sync_write(Sercom* usart, uint16_t* in_data, uint32_t data_length)
{
    #ifdef DEBUG
    if(in_data == NULL || data_length == 0) { return USART_ERROR_INVALID_ARGUMENT; }
    #endif

    uint8_t sc_index = get_sercom_index(usart);
    if(usart_state[sc_index].rx_index < usart_state[sc_index].rx_length)
    {
        return USART_ERROR_BUSY;
    }

    uint16_t* nine = in_data;
    uint8_t* eight = (uint8_t*)in_data;
    bool nine_bit_mode = usart->USART.CTRLB.bit.CHSIZE == USART_CHAR_SIZE_9;

    for(uint32_t i = 0; i < data_length; i++)
    {
        while(!usart->USART.INTFLAG.bit.DRE);
        if(nine_bit_mode)
        {
            usart->USART.DATA.bit.DATA = *nine;
            nine++;
        }
        else
        {
            usart->USART.DATA.bit.DATA = *eight;
            eight++;
        }
    }

    return USART_ERROR_NONE;
}

usart_error usart_sync_read(Sercom* usart, uint16_t* out_data, uint32_t data_length, uint32_t timeout_ms)
{
    #ifdef DEBUG
    if(out_data == NULL || data_length == 0) { return USART_ERROR_INVALID_ARGUMENT; }
    #endif

    uint8_t sc_index = get_sercom_index(usart);
    if(usart_state[sc_index].rx_index < usart_state[sc_index].rx_length)
    {
        return USART_ERROR_BUSY;
    }

    uint32_t count = 0;
    uint16_t* nine = out_data;
    uint8_t* eight = (uint8_t*)out_data;

    bool nine_bit_mode = usart->USART.CTRLB.bit.CHSIZE == USART_CHAR_SIZE_9;
    uint64_t future = delay_get_time_ms() + timeout_ms;
    while(count < data_length)
    {
        if(timeout_ms != 0xFFFFFFFF && delay_get_time_ms() > future)
        {
            return USART_ERROR_TIMEOUT;
        }

        if(usart->USART.STATUS.bit.FERR)
        {
            usart->USART.STATUS.bit.FERR = 1;
            return USART_ERROR_FRAME_ERROR;
        }

        if(usart->USART.STATUS.bit.COLL)
        {
            usart->USART.STATUS.bit.COLL = 1;
            return USART_ERROR_COLLISION_ERROR;
        }

        if(usart->USART.STATUS.bit.PERR)
        {
            usart->USART.STATUS.bit.PERR = 1;
            return USART_ERROR_PARITY_ERROR;
        }

        if(usart->USART.INTFLAG.bit.RXC)
        {
            uint16_t data = usart->USART.DATA.bit.DATA;
            if(nine_bit_mode)
            {
                *nine = data;
                nine++;
            }
            else
            {
                *eight = data;
                eight++;
            }
            count++;
        }
    }

    return USART_ERROR_NONE;
}

usart_error usart_async_write(Sercom* usart, uint16_t* in_data, uint32_t data_length)
{
    #ifdef DEBUG
    if(in_data == NULL || data_length == 0) { return USART_ERROR_INVALID_ARGUMENT; }
    #endif

    __disable_irq();
    uint8_t sc_index = get_sercom_index(usart);

    if(usart_state[sc_index].tx_index < usart_state[sc_index].tx_length)
    {
        __enable_irq();
        return USART_ERROR_BUSY;
    }

    usart_state[sc_index].tx = in_data;
    usart_state[sc_index].tx_length = data_length;
    usart_state[sc_index].tx_index = 0;

    usart->USART.INTENSET.bit.ERROR = 1;
    usart->USART.INTENSET.bit.RXBRK = 1;
    usart->USART.INTENSET.bit.CTSIC = 1;
    usart->USART.INTENSET.bit.RXS = 1;
    usart->USART.INTENSET.bit.RXC = 1;
    usart->USART.INTENSET.bit.TXC = 1;
    usart->USART.INTENSET.bit.DRE = 1;
    NVIC_EnableIRQ(get_sercom_irq(usart));

    __enable_irq();

    return USART_ERROR_NONE;
}

usart_error usart_async_read(Sercom* usart, uint16_t* out_data, uint32_t data_length)
{
    #ifdef DEBUG
    if(out_data == NULL || data_length == 0) { return USART_ERROR_INVALID_ARGUMENT; }
    #endif

    __disable_irq();
    uint8_t sc_index = get_sercom_index(usart);

    if(usart_state[sc_index].rx_index < usart_state[sc_index].rx_length)
    {
        __enable_irq();
        return USART_ERROR_BUSY;
    }

    usart_state[sc_index].rx = out_data;
    usart_state[sc_index].rx_length = data_length;
    usart_state[sc_index].rx_index = 0;

    usart->USART.INTENSET.bit.ERROR = 1;
    usart->USART.INTENSET.bit.RXBRK = 1;
    usart->USART.INTENSET.bit.CTSIC = 1;
    usart->USART.INTENSET.bit.RXS = 1;
    usart->USART.INTENSET.bit.RXC = 1;
    usart->USART.INTENSET.bit.TXC = 1;
    usart->USART.INTENSET.bit.DRE = 1;
    NVIC_EnableIRQ(get_sercom_irq(usart));

    __enable_irq();

    return USART_ERROR_NONE;
}

void usart_cancel_async_write(Sercom* usart)
{
    uint8_t sc_index = get_sercom_index(usart);

    __disable_irq();
    usart->USART.INTENCLR.bit.DRE = 1;
    usart_state[sc_index].tx = NULL;
    usart_state[sc_index].tx_index = 0;
    usart_state[sc_index].tx_length = 0;
    __enable_irq();
}

void usart_cancel_async_read(Sercom* usart, bool give_read_data)
{
    uint8_t sc_index = get_sercom_index(usart);

    __disable_irq();
    if(give_read_data && usart_receive_complete_callback != NULL)
    {
        bool nine_bit_mode = usart->USART.CTRLB.bit.CHSIZE == USART_CHAR_SIZE_9;
        usart_receive_complete_callback(sc_index, usart_state[sc_index].rx, usart_state[sc_index].rx_index, nine_bit_mode);
    }

    usart_state[sc_index].rx = NULL;
    usart_state[sc_index].rx_index = 0;
    usart_state[sc_index].rx_length = 0;
    __enable_irq();
}

void usart_interrupt_handler(Sercom* usart)
{
    uint8_t sc_index = get_sercom_index(usart);

    // Error Handling
    if(usart->USART.INTFLAG.bit.ERROR)
    {
        usart->USART.INTFLAG.reg |= 0x80; // set ERROR bit to clear it

        usart_cancel_async_read(usart, true);
        usart_cancel_async_write(usart);

        // Frame Error
        if(usart->USART.STATUS.bit.FERR)
        {
            usart->USART.STATUS.bit.FERR = 1;
            if(usart_error_callback != NULL)
            {
                usart_error_callback(sc_index, USART_ERROR_FRAME_ERROR);
            }
        }

        // Parity Error
        if(usart->USART.STATUS.bit.PERR)
        {
            usart->USART.STATUS.bit.PERR = 1;
            if(usart_error_callback != NULL)
            {
                usart_error_callback(sc_index, USART_ERROR_PARITY_ERROR);
            }
        }

        // RX Buffer Overflow Error
        if(usart->USART.STATUS.bit.BUFOVF)
        {
            usart->USART.STATUS.bit.BUFOVF = 1;
            if(usart_error_callback != NULL)
            {
                usart_error_callback(sc_index, USART_ERROR_OVERFLOW_ERROR);
            }
        }

        // Collision Error
        if(usart->USART.STATUS.bit.COLL)
        {
            usart->USART.STATUS.bit.COLL = 1;
            if(usart_error_callback != NULL)
            {
                usart_error_callback(sc_index, USART_ERROR_COLLISION_ERROR);
            }
        }

        // Inconsistent Sync Field Error
        if(usart->USART.STATUS.bit.ISF)
        {
            usart->USART.STATUS.bit.ISF = 1;
            if(usart_error_callback != NULL)
            {
                usart_error_callback(sc_index, USART_ERROR_INCONSISTENT_SYNC_FIELD_ERROR);
            }
        }
    }

    if(usart->USART.INTFLAG.bit.RXBRK)
    {
        usart->USART.INTFLAG.reg |= 0x20; // set RXBRK bit to clear it
        if(usart_break_received_callback != NULL)
        {
            usart_break_received_callback(sc_index);
        }
    }

    if(usart->USART.INTFLAG.bit.CTSIC)
    {
        usart->USART.INTFLAG.reg |= 0x10; // set CTSIC bit to clear it
        if(usart_cts_changed_callback != NULL)
        {
            usart_cts_changed_callback(sc_index, usart->USART.STATUS.bit.CTS);
        }
    }

    if(usart->USART.INTFLAG.bit.RXS)
    {
        usart->USART.INTFLAG.reg |= 0x08; // set RXS bit to clear it
        if(usart_receive_started_callback != NULL)
        {
            usart_receive_started_callback(sc_index);
        }
    }

    bool nine_bit_mode = usart->USART.CTRLB.bit.CHSIZE == USART_CHAR_SIZE_9;

    if(usart->USART.INTFLAG.bit.RXC)
    {
        uint16_t data = usart->USART.DATA.bit.DATA;

        if(usart_receive_character_callback != NULL)
        {
            usart_receive_character_callback(sc_index, data, nine_bit_mode);
        }

        if(usart_state[sc_index].rx != NULL)
        {
            uint16_t* nine_rx = usart_state[sc_index].rx;
            uint8_t* eight_rx = (uint8_t*)usart_state[sc_index].rx;
            uint32_t rx_index = usart_state[sc_index].rx_index;

            if(nine_bit_mode)
            {
                nine_rx[rx_index] = data;
            }
            else
            {
                eight_rx[rx_index] = (uint8_t)data;
            }

            if(usart_state[sc_index].rx_index >= usart_state[sc_index].rx_length)
            {
                if(usart_receive_complete_callback != NULL)
                {
                    usart_receive_complete_callback(sc_index, nine_rx, usart_state[sc_index].rx_length, nine_bit_mode);
                }

                usart_state[sc_index].rx = NULL;
                usart_state[sc_index].rx_index = 0;
                usart_state[sc_index].rx_length = 0;
            }
        }
    }

    if(usart->USART.INTFLAG.bit.TXC)
    {
        usart->USART.INTFLAG.reg |= 0x02; // set TXC bit to clear it

        uint32_t tx_index = usart_state[sc_index].tx_index;

        if(tx_index >= usart_state[sc_index].tx_length)
        {
            if(usart_transmit_complete_callback != NULL)
            {
                usart_transmit_complete_callback(sc_index, usart_state[sc_index].tx, usart_state[sc_index].tx_length, nine_bit_mode);
            }

            usart_state[sc_index].tx = NULL;
            usart_state[sc_index].tx_index = 0;
            usart_state[sc_index].tx_length = 0;
        }
    }

    if(usart->USART.INTFLAG.bit.DRE)
    {
        if(usart_state[sc_index].tx != NULL)
        {
            uint16_t* nine_tx = usart_state[sc_index].tx;
            uint8_t* eight_tx = (uint8_t*)usart_state[sc_index].tx;
            uint32_t tx_index = usart_state[sc_index].tx_index;

            if(tx_index < usart_state[sc_index].tx_length)
            {
                if(nine_bit_mode)
                {
                    usart->USART.DATA.bit.DATA = nine_tx[tx_index];
                }
                else
                {
                    usart->USART.DATA.bit.DATA = eight_tx[tx_index];
                }

                usart_state[sc_index].tx_index++;
            }
            else
            {
                // turn off DRE interrupt
                usart->USART.INTENCLR.bit.DRE = 1;
            }
        }
        else
        {
            // turn off DRE interrupt
            usart->USART.INTENCLR.bit.DRE = 1;
        }
    }
}
