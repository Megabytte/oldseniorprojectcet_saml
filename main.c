#include "sam.h"

#include "drivers/defs.h"
#include "drivers/delay.h"
#include "drivers/sysctrl.h"
#include "drivers/gclk.h"
#include "drivers/pm.h"
#include "drivers/gpio.h"
#include "drivers/uart.h"
#include "drivers/spi.h"
#include "drivers/usb_device.h"

#include <inttypes.h>

// Xplained Pro Test Pinout
// PB30 : User LED
// PA15 : Switch
// PA05 : SERCOM0 PAD[1] SPI   CS : CS
// PA06 : SERCOM0 PAD[2] SPI MOSI : SI
// PA04 : SERCOM0 PAD[0] SPI MISO : SO
// PA07 : SERCOM0 PAD[3] SPI  SCK : SCK

void MCU_Init(void);
void MCU_InitClocks(void);

// API TODO:
//     - USB
//     - SPI API IRQ & Async
//     - USART API Debug IRQ & Testing
//     - ???

// TODO: Review APIs for applicable locations for statements like:
// __ISB() "Instruction Synchronization Barrier" = Instruction Pipeline Flush : Code after this statement must be fetched from cache or memory
// __DSB() "Data Synchronization Barrier" = Code execution pauses until all explicit memory accesses are complete
// __DMB() "Data Memory Barrier" = Ensures the order of explicit memory accesses without waiting for their completion

void MCU_InitClocks()
{
    // NOTE: The source for GCLK Generator 0 should get the ONDEMAND bit set before switching it
    sysctrl_gosc_set_on_demand_enabled(CLOCK_OSC8M, true); // default CPU clock at startup, need to set ONDEMAND before switching it

    // VDD 1.62V to 2.7V                                 VDD 2.7V to 3.63V
    // NVM Wait States    Max Operating Frequency        NVM Wait States    Max Operating Frequency
    // 0                  14                             0                  24
    // 1                  28                             1                  48
    // 2                  42
    // 3                  48
    NVMCTRL->CTRLB.bit.RWS = 1; // set flash wait states to 1 as per the chart above

    // setup and enable osc32k
    sysctrl_gosc_set_enabled(CLOCK_OSC32K, false);
    sysctrl_gosc_set_on_demand_enabled(CLOCK_OSC32K, false);
    sysctrl_osc32k_load_factory_calibration();
    sysctrl_osc32k_set_start_up_time(OSC32K_STARTUP_TIME_130);
    sysctrl_gosc32k_set_32k_mode_enabled(CLOCK_32K_OSC32K, true);
    sysctrl_gosc_set_enabled(CLOCK_OSC32K, true);

    while(!sysctrl_osc32k_get_ready_flag());

    // provide DFLL48_REF with osc32k
    gclk_set_gen_division_factor(GCLK_GENERATOR_1, 1);
    gclk_set_gen_source(GCLK_GENERATOR_1, GCLK_SRC_OSC32K);
    gclk_set_gen_improve_duty_cycle_enabled(GCLK_GENERATOR_1, true);
    gclk_set_gen_enabled(GCLK_GENERATOR_1, true);

    gclk_set_clk_source_gen(GCLK_ID_DFLL48, GCLK_GENERATOR_1);

    gclk_set_clk_enabled(GCLK_ID_DFLL48, true);

    // setup and enable dfll48m
    while(!sysctrl_dfll48m_get_ready_flag());
    sysctrl_gosc_set_enabled(CLOCK_DFLL48M, true);

    sysctrl_dfll48m_set_coarse_max_step(31);
    sysctrl_dfll48m_set_fine_max_step(511);
    sysctrl_dfll48m_set_dfll_multiply_factor(1465);

    sysctrl_dfll48m_load_open_loop_coarse_factory_calibration();

    sysctrl_dfll48m_set_closed_loop_mode_enabled(true);
    sysctrl_dfll48m_set_wait_for_lock_enabled(true);

    while(!sysctrl_dfll48m_get_ready_flag());

    // switch the system clock to 48 MHz
    gclk_set_gen_improve_duty_cycle_enabled(GCLK_GENERATOR_0, true);
    gclk_set_gen_source(GCLK_GENERATOR_0, GCLK_SRC_DFLL48M);

    NVIC_SetPriority(SysTick_IRQn, 3);
    SysTick_Config(48000); // SysTick fires every 1 ms

    sysctrl_osc8m_set_prescalar(OSC8M_PRESCALE_1);
    sysctrl_gosc_set_on_demand_enabled(CLOCK_OSC8M, false);

    gclk_set_gen_division_factor(GCLK_GENERATOR_2, 1);
    gclk_set_gen_improve_duty_cycle_enabled(GCLK_GENERATOR_2, true);
    gclk_set_gen_source(GCLK_GENERATOR_2, GCLK_SRC_OSC8M);
    gclk_set_gen_enabled(GCLK_GENERATOR_2, true);

    pm_set_prescalar(PM_CLK_CPU, PM_PRESCALE_1);
    pm_set_prescalar(PM_CLK_APB_A, PM_PRESCALE_1);
    pm_set_prescalar(PM_CLK_APB_B, PM_PRESCALE_1);
    pm_set_prescalar(PM_CLK_APB_C, PM_PRESCALE_1);

    sysctrl_gosc_set_on_demand_enabled(CLOCK_DFLL48M, true);
}

void MCU_Init()
{
    MCU_InitClocks();

    // turn on GPIO and other peripheral clocks
    pm_set_peripheral_clk_enabled(APBB_PORT, true);
    pm_set_peripheral_clk_enabled(APBC_SERCOM0, true);
    pm_set_peripheral_clk_enabled(APBC_SERCOM3, true);
    pm_set_peripheral_clk_enabled(APBB_USB, true);
    pm_set_peripheral_clk_enabled(AHB_USB, true);
    gclk_set_clk_source_gen(GCLK_ID_SERCOMX_SLOW, GCLK_GENERATOR_0);
    gclk_set_clk_source_gen(GCLK_ID_SERCOM0_CORE, GCLK_GENERATOR_0);
    gclk_set_clk_source_gen(GCLK_ID_SERCOM3_CORE, GCLK_GENERATOR_0);
    gclk_set_clk_source_gen(GCLK_ID_USB, GCLK_GENERATOR_0);
    gclk_set_clk_enabled(GCLK_ID_SERCOMX_SLOW, true);
    gclk_set_clk_enabled(GCLK_ID_SERCOM0_CORE, true);
    gclk_set_clk_enabled(GCLK_ID_SERCOM3_CORE, true);
    gclk_set_clk_enabled(GCLK_ID_USB, true);

    gpio_set_io_mode(PA15, GPIO_INPUT); // switch
    gpio_set_input_pull_mode(PA15, GPIO_PULL_UP);
    gpio_set_io_mode(PB30, GPIO_OUTPUT); // led

    // PA05 : SERCOM0 PAD[1] SPI   CS : CS
    // PA06 : SERCOM0 PAD[2] SPI MOSI : SI
    // PA04 : SERCOM0 PAD[0] SPI MISO : SO
    // PA07 : SERCOM0 PAD[3] SPI  SCK : SCK
    gpio_set_alternate_function(PA04, GPIO_AF_D);
    gpio_set_alternate_function(PA06, GPIO_AF_D);
    gpio_set_alternate_function(PA07, GPIO_AF_D);
    gpio_set_alternate_function_enabled(PA04, true);
    gpio_set_alternate_function_enabled(PA06, true);
    gpio_set_alternate_function_enabled(PA07, true);
    // manual SS
    gpio_set_io_mode(PA05, GPIO_OUTPUT);
    gpio_set_level(PA05, GPIO_HIGH);

    spi_set_op_mode(SERCOM0, SPI_OP_MODE_MASTER);
    spi_set_clock_mode(SERCOM0, SPI_CLK_MODE_3);
    spi_set_data_order(SERCOM0, SPI_DATA_ORDER_MSB);
    spi_set_pinout(SERCOM0, SPI_PINOUT_B);
    spi_set_frame_format(SERCOM0, SPI_FORMAT_SPI_FRAME);
    spi_set_character_size(SERCOM0, SPI_CHAR_SIZE_8);
    spi_master_set_baudrate(SERCOM0, 48000000, 10000000);
    volatile uint32_t test = spi_master_get_baudrate(SERCOM0, 48000000);
    spi_master_set_hardware_slave_select_enabled(SERCOM0, false);
    spi_set_enabled(SERCOM0, true);
    spi_set_receiver_enabled(SERCOM0, true);

    // PA22 : SERCOM3 PAD[0] UART TXD (SAM D21 TX line)
    // PA23 : SERCOM3 PAD[1] UART RXD (SAM D21 RX line)
    gpio_set_alternate_function(PA22, GPIO_AF_C);
    gpio_set_alternate_function(PA23, GPIO_AF_C);
    gpio_set_alternate_function_enabled(PA22, true);
    gpio_set_alternate_function_enabled(PA23, true);

    usart_set_clock_source(SERCOM3, USART_CLK_SRC_INTERNAL);
    usart_set_sync_mode(SERCOM3, USART_ASYNCHRONOUS);
    usart_set_pinout(SERCOM3, USART_PINOUT_F);
    usart_set_character_size(SERCOM3, USART_CHAR_SIZE_8);
    usart_set_data_order(SERCOM3, USART_DATA_ORDER_LSB);
    usart_set_stop_bits(SERCOM3, USART_STOP_BITS_1);
    usart_set_frame_format(SERCOM3, USART_FORMAT_USART_FRAME);
    usart_set_oversampling_rate(SERCOM3, USART_SAMPLE_RATE_16X_ARITHMETIC);
    usart_set_oversampling_adjustment_mode(SERCOM3, USART_SAMPLE_RATE_ADJUSTMENT_MODE_0);
    usart_set_baudrate(SERCOM3, 48000000, 115200);
    usart_set_enabled(SERCOM3, true);
    usart_set_transceiver_enabled(SERCOM3, true, true);
    // NOTE: Terminal Software must SET DTR signal for Debugger to enable UART connection

    printf("SPI Speed: %" PRIu32 "\n", test);

    // PA24 : USB D-
    // PA25 : USB D+
    gpio_set_alternate_function(PA24, GPIO_AF_G);
    gpio_set_alternate_function(PA25, GPIO_AF_G);
    gpio_set_alternate_function_enabled(PA24, true);
    gpio_set_alternate_function_enabled(PA25, true);
    // TODO: Call USB API functions




    // TODO: More Stuff!
}

void usart_receive_character_cb(uint8_t sercom_index, uint16_t received_char, bool nine_bit_mode)
{
    usart_cancel_async_write(SERCOM3);
    char buffer[16];
    snprintf(buffer, 16, "Got: %c\n", (char)received_char);
    usart_sync_write(SERCOM3, (uint16_t*)buffer, strlen(buffer));
}

int _write(int _, char* ptr, int len)
{
    usart_sync_write(SERCOM3, (uint16_t*)ptr, len);
    return len;
}

void test_spi(void)
{
    uint8_t dO[2] = { 0x80 | 0x0F, 0x00 };
    uint8_t dI[2] = { 0x00, 0x00 };
    gpio_set_level(PA05, GPIO_LOW);
    spi_master_sync_transfer(SERCOM0, (uint16_t*)dO, (uint16_t*)dI, 2);
    gpio_set_level(PA05, GPIO_HIGH);
    if(dI[1] == 0x68)
    {
        printf("Success!\n");
    }
    else
    {
        printf("Failed!\n");
    }
}

int main(void)
{
    MCU_Init();

    usart_receive_character_callback = usart_receive_character_cb;

    //const char* text = "Hello, World!\n";
    //const uint32_t text_length = strlen(text);
    //uint8_t data = 0;

    for(;;)
    {
        //gpio_set_level(PB30, gpio_read_level(PA15));

        gpio_set_level(PB30, GPIO_HIGH);
        delay_ms(500);
        gpio_set_level(PB30, GPIO_LOW);
        delay_ms(500);
        //test_spi();

        //usart_async_write(SERCOM3, (uint16_t*)text, text_length);
        //if(usart_sync_read(SERCOM3, (uint16_t*)&data, 1, 1) == USART_ERROR_NONE)
        //{
        //    usart_sync_write(SERCOM3, (uint16_t*)&data, 1);
        //}
    }
}

void USB_Handler()
{

}

void SERCOM3_Handler()
{
    usart_interrupt_handler(SERCOM3);
}

void HardFault_Handler()
{
    volatile uint64_t i = 0;
    for(;;)
    {
        i++;
    }
}
